<?php get_header(); ?>
<div class="wrapper">
    <div class="blcIntro">
        <?php mahay_page_title() ;
        if(have_posts()) : while(have_posts()) : the_post(); ?>
        <div class="wow fadeInUp" data-wow-delay="1200ms">
            <?php the_content() ?>
        </div>
        <?php endwhile; endif; ?>
    </div>
    <div class="blcGalerie wow fadeIn" data-wow-delay="300ms">
        <?php get_template_part('template-parts/content/galerie') ?>
    </div>
    <?php if(have_rows('realisations')) : ?>
    <div class="blcSlide wow fadeInUp" data-wow-delay="300ms" >
        <h2 class="s-titre"><?php the_field('rel_titre') ?></h2>
        <div class="slidePartenaires" id="slidePartenaires">
            <?php while(have_rows('realisations')) : the_row('realisations') ?>
            <div class="item clr"> 
                <div class="blcLeft">
                    <div class="img">
                        <?php 
                            $img = get_sub_field('image'); 
                            $image = !empty($img['sizes']['img_real']) ? $img['sizes']['img_real'] : get_theme_file_uri('images/slide-partenaire-4.jpg');
                        ?>
                        <img src="<?php echo $image ?>" alt="<?php bloginfo('name') ?>">
                    </div>
                </div>
                <div class="blcRight">
                    <div class="text">
                        <div class="title"><?php the_sub_field('titre') ?></div>
                        <?php echo wp_trim_words(get_sub_field('description'),70) ?>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
    <?php endif; ?>

    <?php if( get_field('eth_titre')): ?>
    <!-- <div class="textPartenaire">
        <h2 class="titre wow fadeInUp" data-wow-delay="300ms"><?php the_field('eth_titre') ?></h2>
        <div class="wow fadeInUp" data-wow-delay="700ms">
           <?php the_field('eth_contenu') ?>
        </div>
    </div> -->
    <?php endif; ?>

    <?php if( get_field('voy_titre')): ?>
    <!-- <div class="textPartenaire">
        <h2 class="titre wow fadeInUp" data-wow-delay="300ms"><?php the_field('voy_titre') ?></h2>
        <div class="wow fadeInUp" data-wow-delay="700ms">
            <?php the_field('voy_contenu') ?>
        </div>
    </div> -->
    <?php endif; ?>

    <?php if( get_field('res_titre')): ?>
    <div class="textPartenaire">
        <h2 class="titre wow fadeInUp" data-wow-delay="300ms"><?php the_field('res_titre') ?></h2>
        <div class="wow fadeInUp" data-wow-delay="700ms">
          <?php the_field('res_contenu') ?>
        </div>
    </div>
    <?php endif; ?>

</div>
<?php get_footer(); ?>