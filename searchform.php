<?php $unique_id = 'searh_'.rand(0,1); ?>
<div class="blcFormulaire">
    <form method="post" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <div class="container clr wow fadeInUp" data-wow-delay="400ms">
                <div class="blc-chp" style="margin-bottom: 2em">
                    <div class="chp form-group">
                        <label for="<?php echo $unique_id; ?>" class="placeholder"><?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'twentyseventeen' ); ?></label>
                        <input type="search" id="<?php echo $unique_id; ?>" class="search-field form-control" placeholder="" name="s" value="<?php echo get_search_query(); ?>"/>
                    </div>
                </div>
                <div class="btn">
                    <button type="submit" class="search-submit btn-submit link"><?php _e('Envoyer', 'mahay_expedition') ?></button>
                </div>
        </div>
    </form>
</div>