<?php 
    /* Template name: Page Nos Actions */
    get_header(); 

    $args = array(
		'post_type' => 'actions',
		'post_status' =>  'publish',
		'orderby' => 'date',
	);
    $actions = new WP_Query($args);
?>
<div class="pageCircuitL">
    <div class="blcIntro">
        <h1 class="titre wow fadeInUp" data-wow-delay="600ms"><?php the_field('tax_titre') ?> <span class="mahay-2"><?php the_field('tax_stitre') ?></span></h1>
        <div class="wow fadeInUp" data-wow-delay="1200ms">
            <?php the_field('tax_contenu') ?>
        </div>  
    </div>
    
    <div class="wrapper">
        <div class="content-circuit">
            <div class="listeCircuit clr">
                <?php 
                    if( $actions->have_posts()  ):
                        while($actions->have_posts()) : 
                            $actions->the_post(); 
                            $title = get_field('titre');
                            $description = get_field('detail');
                            $image= get_field('image_representative');
                ?>
                        <div class="item wow fadeInUp" dataa-wow-delay="<?php echo $time ?>ms">
                            <div class="content">
                                <div class="blcTitre">
                                    <a href="<?php the_permalink(); ?>" class="s-titre" title="<?php echo $title ?>"><?php echo $title ?></a>
                                </div>
                                <div class="wow fadeInUp" data-wow-delay="1000ms">
                                   <?php echo $description ?>
                                </div>
                                <div class="blcTarif clr">
                                    <a href="<?php the_permalink(); ?>" class="link" title="<?php _e('Découvrir','mahay_expedition') ?>"><?php _e('Découvrir','mahay_expedition') ?></a>
                                </div>
                            </div>
                            <div class="img">
                                <img src="<?php echo $image ?>" alt="<?php echo $title ?>">
                            </div>
                        </div>
                        <?php $time += 400; ?>
                    <?php endwhile; endif; ?>
           
                </div>
        </div>   
    </div>
    
</div>
<?php get_footer(); ?>