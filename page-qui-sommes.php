<?php 
	/* Template name: Page Qui sommes-nous */  
	get_header(); 
	$atout = get_field('atout');
	$description = get_field('description');
	$identite = get_field('identite');
	$cusom_s_titre = get_field('cusom_s_titre');
?>
<div class="wrapper">
    <div class="blcApropos">
    	<?php 
            mahay_page_title();
    	   if(!empty($cusom_s_titre)) : 
        ?>
    	<h2 class="s-titre wow fadeInUp" data-wow-delay="1200ms"><?php echo $cusom_s_titre ?> </h2>
    	<?php endif; ?>
    	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
       	<div class="wow fadeInUp" data-wow-delay="1400ms">
       		<?php the_content(); ?>
      	</div>
	   	<?php endwhile; endif; ?>
    </div>
    <?php if(have_rows('equipe')): ?>
    <div class="blcTeam">
        <div class="slideTeam" id="slideTeam">
        	<?php while(have_rows('equipe')) : the_row(); ?>
	            <div class="item clr">
	                <div class="blcLeft wow fadeInLeft" data-wow-delay="1600ms">
	                    <span class="title"><?php the_sub_field('fonction') ?></span>
	                    <div class="img">
	                    	<?php 
	                    		$img = get_sub_field('image'); 
	                    		$team = !empty($img['sizes']['img_team']) ? $img['sizes']['img_team'] : get_theme_file_uri('images/img-team-2.jpg');
	                    	?>
	                        <img src="<?php echo $team ?>" alt="<?php bloginfo('name') ?>">
	                    </div>
	                </div>
	                <div class="blcRight wow fadeInRight" data-wow-delay="1600ms">
	                    <div class="text">
	                        <div class="name"><?php the_sub_field('nom') ?></div>
	                        <?php
	               			echo force_balance_tags( html_entity_decode( wp_trim_words( htmlentities( get_sub_field('description') ), 60, '...' ) ) );
	                         //echo substr(get_sub_field('description'), 0,414) ?>	                        
	                    </div>
	                </div>
	            </div>
        	<?php endwhile; ?>
        </div>
        <a href="<?php echo get_the_permalink(16) ?>" title="<?php _e('Contactez-nous', 'mahay_expedition') ?>" class="link"><?php _e('contactez-nous', 'mahay_expedition') ?></a>
    </div>
	<?php endif; if(!empty($atout)) : ?>
    <div class="blcVoyage wow fadeInUp" data-wow-delay="600ms">
        <?php echo $atout ?>
    </div> 
	<?php endif; ?>
    <div class="blcSignification">
    	<?php if(!empty($identite)) : 
    		$id_titre = array_column($identite, 'titre'); 
    		$id_desc = array_column($identite, 'description'); 
    	?>
        <div class="content clr">
            <div class="blcLeft wow fadeInLeft" data-wow-delay="600ms">
                <div class="blcLogo">
                    <img src="<?php echo get_theme_file_uri('images/logo-2.jpg') ?>" title="<?php bloginfo('name') ?>">
                </div>
                <div class="blcList">
                    <div class="title"><?php the_field('titre') ?></div>
                    <ul id="listSignification">                    	
                    	<?php foreach($id_titre as $t) : ?>
                        	<li><span><?php echo $t ?></span></li>
                    	<?php endforeach; ?>
                    </ul>
                </div>  
            </div>
            <div class="blcRight wow fadeInRight" data-wow-delay="600ms">
                <div id="signification">
                	<?php foreach($id_desc as $d) : ?>
	                    <div class="item">
	                        <?php echo $d; ?>
	                    </div>
                	<?php endforeach; ?>
                </div>
            </div>
        </div>
        <?php endif; if(!empty($description)) : ?>
        <div class="text wow fadeInUp" data-wow-delay="600ms">
            <?php echo $description; ?>
        </div>      
    	<?php endif; ?>
    </div> 
</div> 
<?php get_footer(); ?>