<?php 
	/* Template name: Page Contact */  
	get_header();
		wp_enqueue_script('captcha');
		wp_enqueue_script('validate');
		$phone = get_theme_mod('opts_phone', '+261 34 15 441 23'); 
		$email = get_theme_mod('opts_email', 'contact@mahayexpedition.com');
		$args = array(
	        'post_type' => 'circuits',
	        'post_status' => 'publish',
	        'posts_per_page' => -1,
	    );
	    $loop = new WP_Query($args);
	?>
	<div class="textContact">
        <?php
            mahay_page_title();        
            if(have_posts()) : while(have_posts()) : the_post(); ?>
        <div class="wow fadeInUp" data-wow-delay="1200ms">
            <?php the_content(); ?>
    	</div>
		<?php endwhile; endif; ?>
    </div>
    <style>
        .blcFormulaire .blc-chp .date .placeholder.date_circuit     { left: 14px }
        .blcFormulaire .form-2 .col                                 { width: 50%; }
        .blcFormulaire .form-2 .blc-chp .chp.textarea               { height: auto; }
        .blcFormulaire .form-2 #message2                            { padding: 25px 14px 10px; height: 59px; }
        @media screen and (max-width: 767px){
            .blcFormulaire .form-2 .col { width: 100%; }
        }
    </style>
  	<div class="blcFormulaire SW_SimpleMakeAppointment style1">
        <div class="wrapper">
        	<form id="form1" action="" method="POST" class="formulaire-contact clr <?php echo !empty($_GET['form']) && $_GET['form'] == 'avis' ? 'form-2' : 'form-3'; if(isset($_GET['objet']) && $_GET['objet'] == 'avis'){ echo ' form-2';}?>">
                <div class="content clr">
                    <div class="blc-chp objet wow fadeInUp" data-wow-delay="1600ms">
                        <div class="chp select">
                            <select name="objet" required="required">
                                <option value="" hidden <?php if(isset($_GET['objet']) && $_GET['objet'] == 'object'){ echo 'selected="selected"';} ?>><?php _e('Objet de la demande', 'mahay_expedition') ?></option>
                                 <option data-option="info" value="infos" selected="selected"><?php _e('Demande d\'informations', 'mahay_expedition') ?></option>
                                <option data-option="avis" value="avis"<?php if(isset($_GET['objet']) && $_GET['objet'] == 'avis'){ echo 'selected="selected"';} ?>><?php _e('Avis clients', 'mahay_expedition') ?></option>

                                <option data-option="resa" value="resa" <?php if(isset($_GET['objet']) && $_GET['objet'] == 'reservation'){ echo 'selected="selected"';} ?>><?php _e('Réservation', 'mahay_expedition') ?></option>
                                <option data-option="dep" value="dep" <?php if(isset($_GET['objet']) && $_GET['objet'] == 'depart'){ echo 'selected="selected"';} ?>><?php _e('Départ garanti', 'mahay_expedition') ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="container clr wow fadeInUp" data-wow-delay="1800ms">
                        <div class="col">
                             <div class="blc-chp resa info dep avis">
                                <div class="chp form-group">
                                    <label for="nom" class="placeholder"><?php _e('Nom', 'mahay_expedition') ?><span>*</span></label>
                                    <input type="text" class="form-control" id="nom" name="nom" required="required">
                                </div>
                            </div>
                            <div class="blc-chp resa info dep avis">
                                <div class="chp form-group">
                                    <label for="prenom" class="placeholder"><?php _e('Prénom', 'mahay_expedition') ?><span>*</span></label>
                                    <input type="text" class="form-control" name="prenom" id="prenom" required="required">
                                </div>
                            </div>
                            <div class="blc-chp avis">
                                <div class="chp form-group">
                                    <label for="circuit2" class="placeholder"><?php _e('Nom du circuit','mahay_expedition') ?><span>*</span></label>
                                    <input  class="form-control" type="text" name="circuit2" value=""  id="circuit2" required="">
                                </div>
                            </div>
                            <div class="blc-chp resa dbl">
                                <div class="chp form-group">
                                    <label for="mail" class="placeholder"><?php _e('E-mail','mahay_expedition') ?><span>*</span></label>
                                    <input type="mail" class="form-control" name="email" value="" id="mail" required="required">
                                </div>
                            </div>
                            <div class="blc-chp resa dbl">
                                <div class="chp form-group">
                                    <label for="telephone" class="placeholder"><?php _e('Téléphone & WhatsApp','mahay_expedition') ?></label>
                                    <input  class="form-control" type="tel" name="telephone" value=""  id="telephone">
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="blc-chp info dep cp">
                                <div class="chp form-group">
                                    <label for="mail2" class="placeholder"><?php _e('E-mail','mahay_expedition') ?><span>*</span></label>
                                    <input type="mail" class="form-control" name="email2" value="" id="mail2" required="required">
                                </div>
                            </div>
                            <div class="blc-chp info dep cp">
                                <div class="chp form-group">
                                    <label for="telephone2" class="placeholder"><?php _e('Téléphone & WhatsApp','mahay_expedition') ?><span>*</span></label>
                                    <input  class="form-control" type="tel" name="telephone2" value=""  id="telephone2">
                                </div>
                            </div>
                            <div class="blc-chp avis">
                                <div class="chp date form-group">
                                    <label for="date_c_debut" class="placeholder date_circuit"><?php _e('Date d\'arrivée','mahay_expedition') ?><span>*</span></label>
                                    <input  class="form-control datepicker" type="text" name="date_c_debut" value=""  id="date_c_debut" required="">
                                </div>
                            </div>
                            <div class="blc-chp avis">
                                <div class="chp date form-group">
                                    <label for="date_c_fin" class="placeholder date_circuit"><?php _e('Date de départ','mahay_expedition') ?><span>*</span></label>
                                    <input  class="form-control datepicker" type="text" name="date_c_fin" value=""  id="date_c_fin" required="">
                                </div>
                            </div>
                            <div class="blc-chp avis">
                                <div class="chp form-group textarea">
                                    <textarea id="message2" name="message2" placeholder="<?php _e('Votre avis', 'mahay_expedition') ?>" required></textarea>
                                </div>
                            </div>
                            <div class="blc-chp resa">
                                <div class="chp select form-group"> 
                                    <label for="pays" class="placeholder">Pays d'origine<span>*</span></label>
                                    <select  name="pays" id="pays" class="form-control form-select" required>
                                        <?php include __DIR__.'/inc/contry-list.php'; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="blc-chp resa">
                                <div class="chp select form-group">
                                    <label for="circuit" class="placeholder"><?php _e('Circuit choisi', 'mahay_expedition') ?><span> *</span></label>
                                    <select  name="circuit" id="circuit" class="form-control form-select" >
                                        <option value="" selected="" hidden="" disabled=""></option>
                                        <?php
                                            while($loop->have_posts()) : $loop->the_post(); global $post;
                                        ?>
                                        <option data-id="<?php echo $post->ID ?>" value="<?php echo $post->post_title ?>" <?php echo !empty($_GET['reservation_id']) && $_GET['reservation_id'] == $post->ID ? 'selected' : ''; ?>><?php echo $post->post_title ?></option>
                                        <?php
                                            endwhile;
                                            //include __DIR__.'/inc/contry-list.php'; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="blc-chp resa">
                                <div class="chp form-group">
                                     <label for="voyageurs" class="placeholder"><?php _e('Nombre de voyageurs', 'mahay_expedition') ?><span>*</span></label>
                                    <input type="number" class="form-control" name="voyageurs" min="1" value=""  id="voyageurs" required="required">
                                </div>
                            </div>
                            <div class="blc-chp resa">
                                <div class="chp select form-group">
                                    <label for="vol" class="placeholder"><?php _e('Souhaite un vol international', 'mahay_expedition') ?><span> *</span></label>
                                    <select  name="vol" id="vol" class="form-control form-select" required>
                                        <option value="" selected hidden disabled></option>
                                        <option value="Oui"><?php _e('Oui','mahay_expedition') ?></option>
                                        <option value="Non"><?php _e('Non', 'mahay_expedition') ?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                             <div class="blc-chp resa">
                                <div class="chp date form-group">
                                    <label for="depart" class="placeholder"><?php _e('Date de départ', 'mahay_expedition') ?><span>*</span></label>
                                    <input type="text" class="form-control datepicker" id="depart" name="depart" autocomplete="off" required="">
                                </div>
                            </div>
                            <div class="blc-chp resa">
                                <div class="chp date form-group">
                                    <label for="arrive" class="placeholder"><?php _e('Date d’arrivée', 'mahay_expedition') ?><span>*</span></label>
                                    <input type="text" class="form-control datepicker"  id="arrive" name="arrive" autocomplete="off" required="">
                                </div>
                            </div>
                            <div class="blc-chp resa info dep">
                                <div class="chp form-group textarea">
                                    <textarea id="message" name="message" placeholder="<?php _e('Votre message', 'mahay_expedition') ?>"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="blcButton wow fadeInUp" data-wow-delay="200ms">
                        <div class="container">
                            <div class="captcha">
                                <div class="blc-captcha"><div id="captcha_resa" class="g-recaptcha" data-sitekey="6Lc1qXAUAAAAAEONeBRl5yUO780ESiSgYUDP78eS"></div></div>
                            </div>
                            <div class="boutton">
                                <div class="btn">
                                    <input class="btn-submit link" value="<?php _e('Envoyer','mahay_expedition') ?>" type="submit">
                                    <span class="ajax-loader"></span>
                                    <input class="reset" value="<?php _e('Reset','mahay_expedition') ?>" type="reset" style="display: none">
                                </div>
                            </div>
                            <div class="form-response"></div>
                        </div>
                    </div>     
                </div>             
            </form>
        </div>
    </div>
    <div class="blcContact">
        <div class="container clr wow fadeInUp" data-wow-delay="400ms">
            <div class="col col-1">
                <a href="<?php echo home_url() ?>" title="<?php bloginfo('name') ?>">
                    <img src="<?php echo get_theme_file_uri('images/logo-contact.png') ?>" alt="<?php bloginfo('name') ?>">
                </a>
            </div>
            <div class="col col-2">
                <div class="blcPicto">
                    <span class="picto"><img src="<?php echo get_theme_file_uri('images/icon-map.svg') ?>" alt="<?php bloginfo('name') ?>"></span>
                    <span class="title">Nous Trouver </span>
                </div>
                <div class="coordonne">
                    Antananarivo - MADAGASCAR
                </div>

            </div>
             <div class="col col-3">
                <div class="blcPicto">
                    <span class="picto"><img src="<?php echo get_theme_file_uri('images/icon-mail.svg') ?>" alt="<?php bloginfo('name') ?>"></span>
                    <span class="title"><?php _e('Nous écrire', 'mahay_expedition') ?></span>
                </div>
                <div class="coordonne">
                    <a href="mailto:<?php echo $email ?>" title="<?php echo $email ?>"><?php echo $email ?></a>
                </div>

            </div>
             <div class="col col-4">
                <div class="blcPicto">
                    <span class="picto"><img src="<?php echo get_theme_file_uri('images/icon-phone.png') ?>" alt="<?php bloginfo('name') ?>"></span>
                    <span class="title"><?php _e('Nous appeler', 'mahay_expedition') ?></span>
                </div>
                <div class="coordonne">
                    <a href="tel:<?php echo preg_replace('/\s+/', '', $phone) ?>" title="<?php echo $phone ?>"><?php echo $phone ?> </a><span><?php _e('(Gsm & WhatsApp)', 'mahay_expedition') ?></span>
                </div>

            </div>
        </div>
    </div>
    <script type="text/javascript">
    	jQuery(document).ready(function($){


            $('.objet select').change(function(){
                var value = $('option:selected', $(this)).attr('data-option');
                $('.formulaire-contact').removeClass('form-2');
                $('.blc-chp').removeClass('hide').removeClass('show');
                $('.blc-chp').not('.'+value).addClass('hide');
                if(value == 'info' || value =='dep'){
                    $('.cp').removeClass('hide').show()
                }else{
                    $('.cp').hide()
                }

                if(value == 'avis'){
                    $('.formulaire-contact').addClass('form-2');
                }
            });

            var value = $('option:selected', $('.objet select')).attr('data-option');
            if(value){
                $('.blc-chp').not('.'+value).addClass('hide');
                if(value == 'info' || value =='dep'){
                        $('.cp').removeClass('hide').show()
                }else{
                        $('.cp').hide()
                }
            }

    		$( ".datepicker" ).datepicker({
                dateFormat : 'dd/mm/yy',
	            onClose: function(date, datepicker){
	                $(this).siblings('.placeholder').hide();
	            }
	        });

    		var $form = $('#form1');
    		if($form.length){
    			$form.validate({
					onfocusout: false,
					rules: {
						telephone: {
							// required: true,
							number: true
						},
                        telephone2: {
                            required: true,
                            number: true
                        }
					},
					highlight: function(element, errorClass, validClass) {
						var $el = $(element);
						$el.parent(".chp").addClass("error");
					},
					unhighlight: function(element, errorClass, validClass) {
						var $el = $(element);
						$el.parent(".chp").removeClass("error");
					},
					submitHandler: function(form) {
						var $form = $(form);
						var data = $form.serialize();
						$.ajax({
							url: mahay_xhr.ajax_url,
							data: {
								'action': 'send_contact',
								'contact': data
							},
							type: 'POST',
							beforeSend: function() {
							    if(grecaptcha.getResponse().length == 0){
							        $('.form-response').addClass('error').empty().append('<p>Merci de valider le captcha de validation</p>');
							        
							        return false;
							    }
								$form.addClass('processing');
							},
							success: function(response) {
								var $o = $('.form-response');
								var r = JSON.parse(response);
								$o.removeClass('success').removeClass('error').addClass(r.status);
								$o.empty().append('<p>'+r.message+'</p>');
								if(r.status == 'success'){
    								setTimeout(function(){
    									var $o = $('.form-response');
    									$o.removeClass('success').removeClass('error').empty();
    									window.location.href = window.location.href
    								}, 4000)
								}
							},
							error: function(error) {
							},
							complete: function(response) {
								$form.removeClass('processing');
								
							}
						});
						return false;
					}
				});
			}
    	})
    </script>
    <!-- Reservation -->
    <?php if(!empty($_GET['reservation_id'])) : ?>
    <script type="text/javascript">
        
        jQuery(document).ready(function($){
            var $resa = $('select#circuit option:selected');
            if($resa.length){
                $('.objet > .select > select').find('option[value="resa"]').prop('selected', 'selected');
                $('.objet > .select > select').trigger('change');
            }
        })

    </script>
    <?php endif ?>
<?php get_footer(); ?>