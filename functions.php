<?php

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

/* Theme Setup */
if ( ! function_exists( 'mahay_expedition_setup' ) ) :

	function mahay_expedition_setup() {

		add_theme_support( 'customize-selective-refresh-widgets' );
		add_theme_support( 'wp-block-styles' );
		add_theme_support( 'align-wide' );
		add_theme_support( 'editor-styles' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails');
		// add_theme_support( 'post-formats', array( 'video',  'standard', 'link' ));

		set_post_thumbnail_size( 150, 150, true );
		add_image_size( 'banner', 1366, 820, true );
		add_image_size( 'banner_page', 1366, 420, true );
		add_image_size( 'bg_testi', 1366, 310, true );
		add_image_size( 'actus_img', 360, 200, true );
		add_image_size( 'single', 722, 460, true );
		add_image_size( 'img_team', 460, 333, true );
		add_image_size( 'img_real', 460, 331, true );
		add_image_size( 'img_srv', 370, 280, true );
		add_image_size( 'gal_fancy', 800, 500, true );
		add_image_size( 'gal_thumb', 380, 230, true );
		add_image_size( 'gal_large', 380, 460, true );
		add_image_size( 'single_actus', 722, 460, true );
		add_image_size( 'img_map', 425, 601, true );

		register_nav_menus(
			array(
				'principal' => __( 'Principal', 'mahay_expedition' ),
			)
		);

		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 197,
				'width'       => 200,
				'flex-width'  => false,
				'flex-height' => false,
			)
		);
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => __( 'Normal', 'mahay_expedition' ),
					'shortName' => __( 'M', 'mahay_expedition' ),
					'size'      => 16,
					'slug'      => 'normal',
				),
				array(
					'name'      => __( 'Medium', 'mahay_expedition' ),
					'shortName' => __( 'L', 'mahay_expedition' ),
					'size'      => 18,
					'slug'      => 'medium',
				),
				array(
					'name'      => __( 'Large', 'mahay_expedition' ),
					'shortName' => __( 'XL', 'mahay_expedition' ),
					'size'      => 48,
					'slug'      => 'large',
				),
			)
		);
		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => __( 'Couleur 1', 'mahay_expedition' ),
					'slug'  => 'c1',
					'color' => '#ad1f09',
				),
				array(
					'name'  => __( 'Couleur 2', 'mahay_expedition' ),
					'slug'  => 'c2',
					'color' => '#262008',
				),
				array(
					'name'  => __( 'Couleur 3', 'mahay_expedition' ),
					'slug'  => 'c3',
					'color' => '#433930',
				),
				array(
					'name'  => __( 'Couleur4', 'mahay_expedition' ),
					'slug'  => 'c4',
					'color' => '#ffffff',
				),
			)
		);
		add_theme_support( 'responsive-embeds' );
	}

endif;
add_action( 'after_setup_theme', 'mahay_expedition_setup' );

/* Widget Init */
if( ! function_exists( 'mahay_expedition_widgets_init' ) ) :

	function mahay_expedition_widgets_init() {

		
		register_sidebar(
			array(
				'name'          => __( 'Footer 1', 'mahay_expedition' ),
				'id'            => 'footer-1',
				'description'   => __( 'Add widgets here to appear in your footer.', 'mahay_expedition' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<div class="titre">',
				'after_title'   => '</div>',
			)
		);

		register_sidebar(
			array(
				'name'          => __( 'Footer 2', 'mahay_expedition' ),
				'id'            => 'footer-2',
				'description'   => __( 'Add widgets here to appear in your footer.', 'mahay_expedition' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<div class="titre">',
				'after_title'   => '</div>',
			)
		);

		register_sidebar(
			array(
				'name'          => __( 'Mention légales', 'mahay_expedition' ),
				'id'            => 'mentions',
				'description'   => __( 'Add widgets here to appear in your legalenotices.', 'mahay_expedition' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2>',
				'after_title'   => '</h2>',
			)
		);

		register_sidebar(
			array(
				'name'          => __( 'Sidebar', 'mahay_expedition' ),
				'id'            => 'sidebar',
				'description'   => __( 'Add widgets here to appear in your sidebar.', 'mahay_expedition' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="titre">',
				'after_title'   => '</h2>',
			)
		);

	}
endif;
add_action( 'widgets_init', 'mahay_expedition_widgets_init' );

if( ! function_exists( 'mahay_expedition_content_width' ) ) :

	function mahay_expedition_content_width() {

		$GLOBALS['content_width'] = apply_filters( 'mahay_expedition_content_width', 1200 );

	}

endif;
add_action( 'after_setup_theme', 'mahay_expedition_content_width', 0 );


if( !function_exists('mahay_expedition_scripts') ) :

	function mahay_expedition_scripts() {

		// CSS
		wp_enqueue_style( 'mahay_expedition-style', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) );
		wp_register_style( 'slick', get_theme_file_uri('css/slick.css'), array(), wp_get_theme()->get( 'Version' ), 'all' );
		wp_register_style( 'animate', get_theme_file_uri('css/animate.css'), array(), wp_get_theme()->get( 'Version' ), 'all' );
		wp_register_style( 'jquery-ui', get_theme_file_uri('css/jquery-ui.min.css'), array(), wp_get_theme()->get( 'Version' ), 'all' );
		wp_register_style( 'reset', get_theme_file_uri('css/reset.css'), array(), wp_get_theme()->get( 'Version' ), 'all' );
		wp_register_style( 'fancybox', get_theme_file_uri('/js/fancybox/jquery.fancybox.min.css'), array(), wp_get_theme()->get( 'Version' ), 'all' );

		wp_enqueue_style('jquery-ui');
		wp_enqueue_style('slick');
		wp_enqueue_style('animate');
		wp_enqueue_style('aos');
		wp_enqueue_style('fancybox');
		wp_enqueue_style('reset');

		// JS
		wp_register_script( 'jquery-ui', get_theme_file_uri('js/jquery-ui.min.js'), array( 'jquery' ), false, true );
		wp_register_script( 'slick', get_theme_file_uri('js/slick.min.js'), array( 'jquery' ), false, true );
		wp_register_script( 'wow', get_theme_file_uri('js/wow.js'), array( 'jquery' ), false, true );
		wp_register_script( 'fancybox', get_theme_file_uri('js/fancybox/jquery.fancybox.min.js'), array( 'jquery' ), false, true );
		wp_register_script( 'isotope', get_theme_file_uri('js/isotop.min.js"'), array( 'jquery','custom' ), false, true );
		wp_register_script( 'imagesLoaded', get_theme_file_uri('js/image-loaded.js"'), array( 'jquery','custom' ), false, true );
		wp_register_script( 'equalheights', get_theme_file_uri('js/jquery.equalheights.min.js'), array( 'jquery' ), false, true );
		wp_register_script( 'validate', get_theme_file_uri('js/jquery.validate.min.js'), array( 'jquery' ), false, true );
		wp_register_script( 'custom', get_theme_file_uri('js/custom.js'), array( 'jquery' ), false, true );
		wp_register_script( 'map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBJwwWxqvXjx_NN1dq-DgGNhleyuZrSSyI&callback=initContactMap', array( ), false, true );
		wp_register_script( 'captcha', 'https://www.google.com/recaptcha/api.js?hl=fr', array( ), false, true );

		// wp_enqueue_script('jquerys');
		wp_enqueue_script('jquery-ui');
		wp_enqueue_script('slick');
		wp_enqueue_script('wow');
		wp_enqueue_script('fancybox');
		wp_enqueue_script('custom');
        wp_localize_script('custom', 'mahay_xhr', array( 'ajax_url' => admin_url( 'admin-ajax.php' )));


	}

endif;
add_action( 'wp_enqueue_scripts', 'mahay_expedition_scripts' );

if( !function_exists('mahay_expedition_skip_link_focus_fix') ) : 

	function mahay_expedition_skip_link_focus_fix() {
		?>
		<script>
		/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
		</script>
		<?php
	}

endif;
add_action( 'wp_print_footer_scripts', 'mahay_expedition_skip_link_focus_fix' );

/* Ajax : Send Contact */
function alefa_mail($to, $subject, array $data, $tpl){           
    $msg = false;
    
    if(!file_exists($tpl)){
        return false;
    }
    extract($data);
    	ob_start();
    include($tpl);
    $content = ob_get_clean(); 

    /*$headers= array(
        'Subject' => $subject,
        'From' => "{$nom} : <{$email}>",
        'Content-Type' => "text/html",
        'charset' => "UTF-8"
    );*/
    // $subject = "Demande d'informations";
    $headers = "From: " . $data['email'] . "\r\n";
    $headers .= "Subject: ".$subject."\r\n";
	$headers .= "Reply-To: ". $data['email'] . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=utf-8\r\n";
    
    //mail($to,"My subject",'fa2');

    if(wp_mail($to,$subject,$content, $headers)){
        $msg = true;
    }

    return $msg;
}


function mahay_log_mailer_errors( $wp_error ){
  $fn = ABSPATH . '/mail.log'; // say you've got a mail.log file in your server root
  $fp = fopen($fn, 'a');
  fputs($fp, "Mailer Error: " . $wp_error->get_error_message() ."\n");
  fclose($fp);
}
// add_action('wp_mail_failed', 'mahay_log_mailer_errors', 10, 1);

function fct_ajax_send_contact(){
	$contact = array(); parse_str($_POST["contact"],$contact);
	$objet = $contact['objet'];
	$to = get_bloginfo('admin_email');
	if($objet != "avis"){
		switch ($objet) {
			case 'infos':
				$subject = __('Demande d\'informations', 'mahay_expedition');
				$tpl = __DIR__.'/template-parts/email/tpl-information.php';
				$contact['telephone'] = $contact['telephone2'];
				$contact['email'] = $contact['email2'];
				break;
			case 'dep':
				$subject = __('Depart garanti', 'mahay_expedition');
				$tpl = __DIR__.'/template-parts/email/tpl-depart.php';
				$contact['telephone'] = $contact['telephone2'];
				$contact['email'] = $contact['email2'];
				break;
			
			default:
				$subject = __('Réservation', 'mahay_expedition');
				$tpl = __DIR__.'/template-parts/email/tpl-reservation.php';
				break;
		}

		if($tpl){
			$status = alefa_mail($to, $subject, $contact, $tpl);
			$response = $status === true ? array('status' => 'success', 'message' => __('Merci pour votre message. Il a été envoyé', 'mahay_expedition')) : array('status' => 'error', 'message' => __("Une erreur s'est produite lors de la tentative d'envoi de votre message. Veuillez réessayer plus tard.", 'mahay_expedition'));
			print_r(json_encode($response));
			die;
		}		
	}else{
		
		$nom = sanitize_text_field($contact['nom']);
		$prenom = sanitize_text_field($contact['prenom']);
		$circuit = sanitize_text_field($contact['circuit2']);
		$date_ariv = sanitize_text_field($contact['date_c_debut']);
		$date_dep = sanitize_text_field($contact['date_c_fin']);
		$avis = sanitize_text_field($contact['message2']);
		$name = "$nom $prenom";

		$post_id = wp_insert_post(array(
			'post_title' => $name,
			'post_name' => sanitize_title($name),
			'post_author' => 4,
			'post_type' => 'temoignages',
			'post_status' => 'draft',
			'meta_input' => array(
				'titre' => $circuit,
				'description' => $avis, 
				'date_ariv' => $date_ariv, 
				'date_dep' => $date_dep 
			)
		), false);
		$response = $post_id ? array('status' => 'success', 'message' => __('Merci pour votre avis. Il a été envoyé', 'mahay_expedition')) : array('status' => 'error', 'message' => __("Une erreur s'est produite lors de la tentative d'envoi de votre avis. Veuillez réessayer plus tard.", 'mahay_expedition'));
			print_r(json_encode($response));
			die;
	}
	exit();
}
add_action( 'wp_ajax_send_contact', 'fct_ajax_send_contact' );
add_action( 'wp_ajax_nopriv_send_contact', 'fct_ajax_send_contact' );

remove_filter( 'pre_term_description', 'wp_filter_kses' );
remove_filter( 'pre_link_description', 'wp_filter_kses' );
remove_filter( 'pre_link_notes', 'wp_filter_kses' );
remove_filter( 'term_description', 'wp_kses_data' );

require get_template_directory() . '/inc/menu-walker.php';
require get_template_directory() . '/inc/template-functions.php';
require get_template_directory() . '/inc/recent-posts-widget.php';
require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/inc/breadcrumbs.php';

add_action( 'wp_print_styles', function() {
    wp_dequeue_style( 'wp-block-library-css' );
    wp_deregister_style( 'wp-block-library-css' );
}, 100 );

/*
// supprimer les notifications du core
add_filter( 'pre_site_transient_update_core', create_function( '$a', "return null;" ) );
// supprimer les notifications de thèmes
remove_action( 'load-update-core.php', 'wp_update_themes' );
add_filter( 'pre_site_transient_update_themes', create_function( '$a', "return null;" ) );
// supprimer les notifications de plugins
remove_action( 'load-update-core.php', 'wp_update_plugins' );
add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );
*/