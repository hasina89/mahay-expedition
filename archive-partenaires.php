<?php get_header();?>

	<div class="blcPartenaire PageActuL archive-partenaire">
		<div class="wrapper">
			<h1 class="titre wow fadeInUp p15" data-wow-delay="600ms" ><?php _e('Nos partenaires', 'mahay_expedition') ?></h1>
		</div>
	    <div class="listPartenaire wrapper wow fadeIn" data-wow-delay="1800ms">
	        <div class="listItem sliderPartenaire clr">
	        	<?php 
	        		if(have_posts()) {
	        			while(have_posts()){
	        				the_post();
	        				get_template_part('template-parts/content/item', 'partenaire');
	        			} 

	        		}
	         	?>
	        </div>
	    </div>
	</div>	
	<script type="text/javascript">
    jQuery(document).ready(function($){

        setTimeout(function(){
            $('html, body').animate({ scrollTop: $('.bredcrumb').offset().top }, 1000, "linear");
        }, 2000);

    });
</script>

<?php get_footer(); ?>