<?php 
	/* Template name: Page Actualités */ 
	get_header(); 
	wp_enqueue_script('equalheights'); 
	$args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'desc',
		'posts_per_page' => -1
	);
	$loop = new WP_Query($args);
?>

	<div class="blcIntro">
        <?php
        	mahay_page_title();
        	if(have_posts()) : while(have_posts()) : the_post(); ?>
        <div class="wow fadeInUp" data-wow-delay="1200ms">
            <?php the_content(); ?>
        </div>
        <?php endwhile; endif; ?>
    </div>
	<div class="listesActu wrapper wow fadeIn" data-wow-delay="1800ms">
	        <div class="slideActu" id="slideActu">
	        	<?php 
	        		if($loop->have_posts()) {

	        			while($loop->have_posts()){
	        				$loop->the_post();
	        				get_template_part('template-parts/content/item', 'actus');
	        			} 

	        		}
	         	?>
	        </div>
	    </div>	

<?php get_footer(); ?>