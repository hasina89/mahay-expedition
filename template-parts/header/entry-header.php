<?php $phone = get_theme_mod('opts_phone', '+261 34 15 441 23'); ?>
<div class="blcTop clr">
	<div class="blcCoord">
	    <div class="tel">
	        <a href="tel:<?php echo preg_replace('/\s+/', '', $phone) ?>"><?php echo $phone ?> </a><span><?php _e('(Gsm & WhatsApp)','mahay_expedition') ?></span>
	    </div>
	    <div class="language">
	    	<?php echo do_shortcode('[google-translator]'); ?>
	       <!--  <ul>
	           <li class="fr"><a href="#" title="Français">Français</a></li>
	           <li class="hide"><a href="#" title="Français">Anglais</a></li>
	       </ul> -->
	    </div>
	</div>
</div>