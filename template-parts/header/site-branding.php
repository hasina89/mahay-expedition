<a  href="<?php echo home_url(); ?>" class="logo" title="<?php bloginfo('name'); ?>">
    <img src="<?php echo get_theme_mod( 'header_logo', get_theme_file_uri('images/logo.png') ); ?>" alt="<?php bloginfo('name'); ?>">
</a>