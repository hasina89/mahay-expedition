<div class="footer-1">
	<a href="<?php echo home_url(); ?>" class="logo-f" title="<?php bloginfo('name'); ?>">
		<img src="<?php echo get_theme_mod( 'header_logo_f', get_theme_file_uri('images/logo-f.png') ); ?>" alt="<?php bloginfo('name'); ?>">
	</a>
</div>
<div class="footer-2">
	<?php dynamic_sidebar('footer-1') ?>
</div>
<div class="footer-3">
	<?php dynamic_sidebar('footer-2') ?>    			
</div>
<div class="social">
	<a href="<?php echo get_theme_mod( 'sociale_fb', 'https://www.facebook.com/MahayExpedition' );?>" title="<?php _e('Facebook', 'mahay_expedition') ?>" class="fb" target="_blank"><?php _e('Facebook', 'mahay_expedition') ?></a>
	<a href="<?php echo get_theme_mod( 'sociale_gp', 'https://plus.google.com/+Stéphanethamin' );?>" title="<?php _e('Google plus', 'mahay_expedition') ?>" class="google" target="_blank"><?php _e('Google plus', 'mahay_expedition') ?></a>
	<a href="<?php echo get_theme_mod( 'sociale_tw', 'https://twitter.com/MahayExpedition' );?>" title="<?php _e('Twitter','mahay_expedition') ?>" class="twitter" target="_blank"><?php _e('Twitter','mahay_expedition') ?></a>
	<a href="header" title="<?php _e('Haut') ?>" class="scrollTop scroll"></a>
</div>