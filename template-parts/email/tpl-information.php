<!DOCTYPE html>
<html>
<head lang="en">
	<title>Mahay Expédition | Demande d'informations</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<style type="text/css">
		body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} 
		table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} 
		img{-ms-interpolation-mode: bicubic;} 
		img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;}
		table{border-collapse: collapse !important;}
		body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}
		a[x-apple-data-detectors] {
			color: inherit !important;
			text-decoration: none !important;
			font-size: inherit !important;
			font-family: inherit !important;
			font-weight: inherit !important;
			line-height: inherit !important;
		}
		@media screen and (max-width: 525px) {
			.wrapper			{ width: 100% !important; max-width: 100% !important; }
			.logo img 			{ margin: 0 auto !important; }
			.mobile-hide 		{ display: none !important; }
			.img-max 			{ max-width: 100% !important; width: 100% !important; height: auto !important; }
			.responsive-table 	{ width: 100% !important; }
			.padding 			{ padding: 10px 5% 15px 5% !important; }
			.padding-meta 		{ padding: 30px 5% 0px 5% !important; text-align: center; }
			.padding-copy 		{ padding: 10px 5% 10px 5% !important; text-align: center; }
			.no-padding 		{ padding: 0 !important; }
			.section-padding 	{ padding: 50px 15px 50px 15px !important; }
			.mobile-button-container { margin: 0 auto; width: 100% !important; }
			.mobile-button 		{ padding: 15px !important; border: 0 !important; font-size: 16px !important; display: block !important; }
			.title-detail		{ padding-bottom:30px !important }
		}
		@media screen and (max-width: 475px) {
			.text-prod			{ font-size:30px !important; padding-top:0 !important }
		}
		div[style*="margin: 16px 0;"] { margin: 0 !important; }
	</style>
	<!--[if gte mso 12]>
	<style type="text/css">
	.mso-right {
		padding-left: 20px;
	}
	</style>
	<![endif]-->
</head>
<body style="margin: 0 !important; padding: 0 !important;">
	<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0" style="width:100%; background-color:#fafafa">
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center" style="width: 100%; max-width:600px; margin: 0 auto">
				<tr>
					<td bgcolor="#ffffff" align="center">
						<!--[if (gte mso 9)|(IE)]>
						<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
						<tr>
						<td align="center" valign="top" width="500">
						<![endif]-->
						<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="wrapper">
							<tr>
								<td align="center" valign="top" style="padding: 15px 0;" class="logo">
									<img alt="Logo" src="http://mahay-expedition.maki-group.mg/wp-content/themes/mahay-expedition/images/logo-contact.png" style="display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;" border="0">
								</td>
							</tr>
						</table>
						<!--[if (gte mso 9)|(IE)]>
						</td>
						</tr>
						</table>
						<![endif]-->
					</td>
				</tr>
				<tr>
					<td class="section-padding">
						<!--[if (gte mso 9)|(IE)]>
						<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
						<tr>
						<td align="center" valign="top" width="500">
						<![endif]-->
						<table  border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;" class="responsive-table">
							<tr>
								<td bgcolor="#c3312d" align="center" style="padding: 50px 15px 50px 15px;">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">
										<tr>
											<td>
												<!-- HERO IMAGE -->
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													</tr>
													<tr>
														<td>
															<!-- COPY -->
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td align="center" style="font-size: 30px; font-family: Helvetica, Arial, sans-serif; color: #fff; padding-top: 5px;" class="padding-copy text-prod">Demande d'informations</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!--[if (gte mso 9)|(IE)]>
						</td>
						</tr>
						</table>
						<![endif]-->
					</td>
				</tr>
				<tr>
					<td bgcolor="#ffffff" align="center" style="padding: 25px 15px 40px 15px;" class="section-padding">
						<table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>
												<!-- COPY -->
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 10px; padding-bottom:30px" class="padding-copy title-detail">Détail de la demande</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="center">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 500px;" class="responsive-table">
										<tbody>
											<tr>
												<td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">
													<table cellspacing="0" cellpadding="0" border="0" width="100%">
														<tbody>
														<tr>
															<td valign="top" class="mobile-wrapper">
																<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
																	<tbody><tr>
																		<td style="padding: 0 0 10px 0;">
																			<table cellpadding="0" cellspacing="0" border="0" width="100%">
																				<tbody><tr>
																					<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Nom</td>
																				</tr>
																			</tbody></table>
																		</td>
																	</tr>
																</tbody></table>
																<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
																	<tbody><tr>
																		<td style="padding: 0 0 10px 0;">
																			<table cellpadding="0" cellspacing="0" border="0" width="100%">
																				<tbody><tr>
																					<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"><?php echo $nom ?></td>
																				</tr>
																			</tbody></table>
																		</td>
																	</tr>
																</tbody></table>
															</td>
														</tr>
														</tbody>
													</table>
												</td>
                                            </tr>
											<tr>
												<td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">
													<table cellspacing="0" cellpadding="0" border="0" width="100%">
														<tbody>
														<tr>
															<td valign="top" class="mobile-wrapper">
																<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
																	<tbody><tr>
																		<td style="padding: 0 0 10px 0;">
																			<table cellpadding="0" cellspacing="0" border="0" width="100%">
																				<tbody><tr>
																					<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Prénom</td>
																				</tr>
																			</tbody></table>
																		</td>
																	</tr>
																</tbody></table>
																<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
																	<tbody><tr>
																		<td style="padding: 0 0 10px 0;">
																			<table cellpadding="0" cellspacing="0" border="0" width="100%">
																				<tbody><tr>
																					<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"><?php echo $prenom ?></td>
																				</tr>
																			</tbody></table>
																		</td>
																	</tr>
																</tbody></table>
															</td>
														</tr>
														</tbody>
													</table>
												</td>
                                            </tr>
											<tr>
												<td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">
													<table cellspacing="0" cellpadding="0" border="0" width="100%">
														<tbody>
														<tr>
															<td valign="top" class="mobile-wrapper">
																<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
																	<tbody><tr>
																		<td style="padding: 0 0 10px 0;">
																			<table cellpadding="0" cellspacing="0" border="0" width="100%">
																				<tbody><tr>
																					<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">E-mail</td>
																				</tr>
																			</tbody></table>
																		</td>
																	</tr>
																</tbody></table>
																<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
																	<tbody><tr>
																		<td style="padding: 0 0 10px 0;">
																			<table cellpadding="0" cellspacing="0" border="0" width="100%">
																				<tbody><tr>
																					<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"><a href="mailto:<?php echo $email ?>" style="text-decoration: none; color: #333333"><?php echo $email ?></a></td>
																				</tr>
																			</tbody></table>
																		</td>
																	</tr>
																</tbody></table>
															</td>
														</tr>
														</tbody>
													</table>
												</td>
                                            </tr>
											<tr>
												<td style="padding: 10px 0 0 0; border-top: 1px dashed #aaaaaa;">
													<table cellspacing="0" cellpadding="0" border="0" width="100%">
														<tbody>
														<tr>
															<td valign="top" class="mobile-wrapper">
																<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="left">
																	<tbody><tr>
																		<td style="padding: 0 0 10px 0;">
																			<table cellpadding="0" cellspacing="0" border="0" width="100%">
																				<tbody><tr>
																					<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;">Téléphone</td>
																				</tr>
																			</tbody></table>
																		</td>
																	</tr>
																</tbody></table>
																<table cellpadding="0" cellspacing="0" border="0" width="47%" style="width: 47%;" align="right">
																	<tbody><tr>
																		<td style="padding: 0 0 10px 0;">
																			<table cellpadding="0" cellspacing="0" border="0" width="100%">
																				<tbody><tr>
																					<td align="right" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px;"><?php echo $telephone ?></td>
																				</tr>
																			</tbody></table>
																		</td>
																	</tr>
																</tbody></table>
															</td>
														</tr>
														</tbody>
													</table>
												</td>
                                            </tr>                                   
											<tr>
												<td valign="top" class="mobile-wrapper" style="padding: 10px 0 0 0; border-top: 1px solid #eaeaea; border-bottom:0">
													<!-- LEFT COLUMN -->
													<table cellpadding="0" cellspacing="0" border="0" width="100%" style="width: 100%;" align="left">
														<tbody><tr>
															<td style="padding: 0 0 0 0;">
																<table cellpadding="0" cellspacing="0" border="0" width="100%">
																	<tbody><tr>
																		<td align="left" style="font-family: Arial, sans-serif; color: #333333; font-size: 16px; font-weight: normal;">Message</td>
																	</tr>
																</tbody></table>
															</td>
														</tr>
													</tbody></table>
													<!-- RIGHT COLUMN -->
												</td>
											</tr>
											<tr>
												<td valign="top" class="mobile-wrapper" style="padding: 0 0 0 0; border-top: 0; border-bottom:1px dashed #aaaaaa">																
													<table cellpadding="0" cellspacing="0" border="0" width="100%" style="width: 100%; margin-top:5px;" align="left">
														<tbody>
															<tr>
															<td>
																<table cellpadding="0" cellspacing="0" border="0" width="100%">
																	<tbody><tr>
																		<td align="left" style="font-family: Arial, sans-serif; font-size:14px; color:#666666">
																			<p style="margin-bottom:14px; margin-top: 0; line-height:24px"><?php echo stripslashes($message); ?></p>
																		</td>
																	</tr>
																</tbody></table>
															</td>
														</tr>
													</tbody></table>
												</td>
											</tr>
                                        </tbody>
                                    </table>
                                </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td bgcolor="#ffffff" align="center" style="padding: 20px 0px;">
						<!--[if (gte mso 9)|(IE)]>
						<table align="center" border="0" cellspacing="0" cellpadding="0" width="500">
						<tr>
						<td align="center" valign="top" width="500">
						<![endif]-->
						<!-- UNSUBSCRIBE COPY -->
						<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 500px;" class="responsive-table">
							<tr>
								<td align="center" style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;">
								Antananarivo - MADAGASCAR
									<br>
									<a href="tel:0341544123" style="color: #666666; text-decoration: none;" title="+261 34 15 441 23">+261 34 15 441 23</a>
									<span style="font-family: Arial, sans-serif; font-size: 12px; color: #444444;">&nbsp;&nbsp;|&nbsp;&nbsp;</span>
									<a href="mailto:contact@mahayexpedition.com" style="color: #666666; text-decoration: none;" title="contact@mahayexpedition.com">contact@mahayexpedition.com</a>
								</td>
							</tr>
						</table>
						<!--[if (gte mso 9)|(IE)]>
						</td>
						</tr>
						</table>
						<![endif]-->
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</body>
</html>
