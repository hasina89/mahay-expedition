<?php 

	$duree = get_field('duree'); 

	$photos = get_field('photos'); 

	$parcours = get_field('parcours'); 

    $carte = get_field('carte'); 

    $img_map = get_field('map'); 

    $zoom = get_field('map_zoom'); 

    $map = !empty($img_map) ? !empty($img_map['sizes']['img_map']) ? $img_map['sizes']['img_map'] : $img_map['url'] : '';
    $map_zoom = !empty($zoom) ? $zoom : $map;

?>

<style>
    @media screen and (max-width: 900px){
        .link-resa{
            right: 170px !important;
        }

    }
    @media screen and (max-width: 768px){
        .link-resa{
            top: 0 !important;
            right: 0 !important;
            bottom: auto !important;
            position: relative !important;
            width: 100%;
            text-align: center;
            margin-top: 1rem;
        }
        .pageCircuitD .circuitInfo .blcTarif{
            margin-bottom: 0
        }
    }
</style>
<div class="blcIntro">

    <?php mahay_page_title(); ?>

    <div class="wow fadeInUp" data-wow-delay="1200ms">

    	<?php the_content(); ?>

    </div>

</div>



<div class="circuitInfo">

    <div class="slideParcours">

        <div class="item clr">

            <div class="blcLeft wow fadeInLeft" data-wow-delay="1600ms">

                 <div class="blcTarif clr">

                    <div><?php _e('A partir de', 'mahay_expedition') ?></div>

                    <div><span><b><?php the_field('prix') ?><sup>€</sup></b></span>/ <?php _e('personne','mahay_expedition') ?></div>
                    <a href="<?php echo home_url('/contact/?reservation_id='.get_the_ID()); ?>" class="link link-resa" title="<?php _e('Réserver', 'mahay_expedition') ?>" tabindex="0" style="top: 0; right: 100px; bottom: auto;"><?php _e('Réserver', 'mahay_expedition') ?></a>

                </div>

                <div class="img" id="slideParcours">

                	<?php if(!empty($photos)) : foreach($photos as $p ) : ?>

                    	<img src="<?php echo !empty($p['sizes']['img_team']) ? $p['sizes']['img_team'] : $p['url']; ?>" alt="<?php the_title() ?>">

                	<?php endforeach; else :?>

                    <img src="<?php echo get_theme_file_uri('images/circuit-1.jpg'); ?>" alt="<?php the_title() ?>">

                	<?php endif; ?>

                </div>

            </div>

            <div class="blcRight wow fadeInRight" data-wow-delay="1600ms">

                <div class="text">

                    <div class="s-titre"><?php _e('Informations sur le circuit', 'mahay_expedition') ?></div>

                   	<ul>

                    	<li><b><?php _e('Destination', 'mahay_expedition') ?> </b>: <?php echo strip_tags(get_field('destination')) ?></li>

                        <li><b><?php _e('Durée', 'mahay_expedition') ?> </b>: <?php echo $duree['d_jours'].'&nbsp;'.__('Jours', 'mahay_expedition') ?>  / <?php echo $duree['d_nuits'].'&nbsp;'.__('Nuits', 'mahay_expedition') ?></li>

                        <li><b><?php _e('Atout', 'mahay_expedition') ?></b> : <?php echo strip_tags(get_field('atout')) ?></li>

        
                   	</ul>    
                    

                </div>

            </div>

            <?php if(empty($map)) : if(empty($carte)) : ?>

            <a href="#" title="<?php _e('Voir le parcours', 'mahay_expedition') ?>" class="link parcours wow fadeInRight" data-wow-delay="1600ms"><?php _e('Voir le parcours', 'mahay_expedition') ?></a>

            <?php else : ?>

            <a href="<?php echo $carte['url'] ?>" title="<?php _e('Voir le parcours', 'mahay_expedition') ?>" class="link wow fadeInRight  fancybox" data-wow-delay="1600ms" data-fancybox="groupe1" rel="1"><?php _e('Voir le parcours', 'mahay_expedition') ?></a>

            <?php endif; endif; ?>

        </div>

    </div>

</div>



<div class="textParcours wow fadeInUp <?php echo !empty($carte) ? 'with-map' : 'no-map' ?>" data-wow-delay="400ms" data-test="page">

    <?php if(empty($map)) : ?>

    <h2 class="titre"><?php _e('Parcours', 'mahay_expedition') ?></h2>

    <div class="entry-content">

        <?php the_field('description') ?>

    </div>

    <?php else : ?>

    <div class="par_cols">
        <div class="col_left">
            <h2 class="titre"><?php _e('Parcours', 'mahay_expedition') ?></h2>

            <div class="entry-content">

                <?php the_field('description') ?>

            </div>
        </div>
        <div class="col_right">

            <div class="wrap-img">
             <a href="<?php echo $map_zoom; ?>" title="<?php the_title(); ?>" class="fancybox" data-fancybox="groupe1" rel="1">
               <img src="<?php echo $map ?>" alt="<?php the_title(); ?>">
            </a>

            </div>
        </div>
    </div>
    <?php endif; ?>

</div>



<?php if(!empty($parcours)) :  $count =  count($parcours); ?>

<div class="slideCircuit wow fadeInUp" data-wow-delay="600ms">

	<div id="slideCircuit">

		<?php foreach($parcours as $p) :  ?>

			<div class="item">

	            <div class="blcText">

	                <div class="jour">

	                    <span><?php _e('j','mahay_expedition') ?><em><?php echo strip_tags($p['jour']) ?></em></span>
                        <span class="jourTo"><?php _e('au', 'mahay_expedition') ?></span>
                        <span><?php _e('j', 'mahay_expedition') ?><em><?php echo strip_tags($p['jour_2']) ?></em></span>

	                </div>

	               <h3 class="s-titre"><?php echo strip_tags($p['titre']) ?></h3> 

	               <div class="entry-content">

	               		<?php

	               			echo $p['description']; 
                            // echo force_balance_tags( html_entity_decode( wp_trim_words( htmlentities( $p['description'] ), 38, '...' ) ) ); 

                        ?>

	               </div>

	            </div>

	            <div class="img">

	            	<?php $img = !empty($p['photo']['sizes']['actus_img']) ? $p['photo']['sizes']['actus_img'] : get_theme_file_uri('images/slidecircuit-1.jpg'); ?>

	                <img src="<?php echo $img ?>" alt="<?php echo strip_tags($p['titre']) ?>">

	            </div>

	        </div>

	    <?php endforeach; ?>

	</div>

</div>

<?php endif; ?>