<?php 
    global $post;  
	$link = get_the_permalink();
	$title = get_the_title();
    $thumb = get_field('post_humbnail', $post->ID);
    if(!empty($thumb)){
        $actus_img = !empty($thumb['sizes']['actus_img']) ? $thumb['sizes']['actus_img'] : get_theme_file_uri('images/img-actu-3.jpg');
    }else{
        $actus_img = has_post_thumbnail() ? get_the_post_thumbnail_url($post->ID, 'actus_img') : get_theme_file_uri('images/img-actu-3.jpg');
    }
	$excerpt = wp_trim_words( get_the_excerpt(), 17, "..." );
?>
<div class="item">
    <div class="container clr">
        <div class="date">
            <?php mahay_the_date(); ?>
        </div>
        <div class="actu">
            <a href="<?php echo $link ?>" class="s-titre" title="<?php echo $title ?>"><?php echo $title ?></a>
            <p><?php echo $excerpt; ?></p>
        </div>
    </div>
    <div class="img-actu">
       <a href="<?php echo $link ?>" title="<?php echo $title ?>">
            <img src="<?php echo $actus_img ?>" alt="<?php echo $title ?>">
            <span class="link"><?php _e('En savoir plus', 'mahay_expedition') ?></span>
        </a>
    </div>
</div>