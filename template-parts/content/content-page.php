<div class="pageMada">
	<div class="wrapper">
		<article class="page-content">
			<header class="entry-header blcIntro">
				<?php mahay_page_title() ?>
			</header>
			<?php the_content() ?>
		</article>
	</div>
</div>