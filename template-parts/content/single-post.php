<?php 

	$format = get_field('format_de_larticle');

    $post_humbnail = get_field('post_humbnail');

    $photos = get_field('photos'); 

	$video_url = get_field('url');



?>

<div class="blcLeft">

    <div class="blcTitre wow fadeIn" data-wow-delay="600ms">

        <div class="date">

            <?php mahay_the_date() ?>

        </div> 

        <h1 class="titre"><?php the_title(); ?></h1>     

    </div>

    <div class="blcVideo wow fadeIn" data-wow-delay="1200ms"> 

    <?php 

    	if($format == "video" && !empty($video_url)) {

    		echo($video_url);

    	} else{

    			$image = !empty($post_humbnail['sizes']['single_actus']) ? $post_humbnail['sizes']['single_actus'] : $post_humbnail['url'];

    		?>

    			<figure>

    				<img src="<?php echo $image ?>" alt="<?php bloginfo('name') ?>">

    			</figure>

    		<?php

    	}

    ?>

    </div>

    <div class="blcText wow fadeIn" data-wow-delay="400ms">

       <?php the_content() ?>

    </div>
    <?php
    if(is_array( $photos )) :
        if(!empty($photos) || count($photos)%3 == 1) : ?>
    <div class="wrap-images">
        <div class="clr">
        <?php foreach($photos as $p) : ?>
            <div class="item">
                <img src="<?php echo $p['sizes']['actus_img'] ?>">
            </div>
        <?php endforeach; ?>
        </div>
    </div>
    <?php endif; ?>
    <?php endif; ?>
    <div class="blcText wow fadeIn" data-wow-delay="800ms">

       <?php the_field('contenu') ?>

    </div>

</div>