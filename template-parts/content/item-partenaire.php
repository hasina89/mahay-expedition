<?php 
    global $post;  
    $link = get_the_permalink();
    $title = get_the_title();
    $img = get_field('image');
    $actus_img = !empty($img['sizes']['actus_img']) ? $img['sizes']['actus_img'] : get_theme_file_uri('images/assoc-1.jpg');
    $excerpt = wp_trim_words( get_the_excerpt(), 24, "..." );
?>
<div class="item">
    <div class="text">
        <a href="<?php echo $link ?>" class="title" title="<?php echo $title ?>"><?php echo $title ?></a>
        <p>
            <?php echo $excerpt ?>  
        </p>
        <a href="<?php echo $link ?>" title="<?php _e('Découvrir', 'mahay_expedition') ?>" class="link"><?php _e('Découvrir', 'mahay_expedition') ?></a>
    </div>
    <div class="img">
        <img src="<?php echo $actus_img ?>" alt="<?php echo $title ?>">
    </div>
</div>