<div class="item">
    <div>
        <div class="blcImg">
            <?php the_term_thumbnail( $t->term_id,'img_srv'); $link = get_term_link($t, 'themes'); ?>
        </div>
        <a href="<?php echo $link ?>" class="titre"><?php echo $t->name ?></a>
        <div class="hide">
            <div class="content">
                <h2 class="titre"><?php echo $t->name ?></h2>
                <p><?php echo ($t->description) ?></p>
                <a href="<?php echo $link ?>" class="link" title="<?php _e('Découvrir', 'mahay_expedition') ?>"><?php _e('découvrir', 'mahay_expedition') ?></a>
            </div>
        </div>
    </div>
</div>