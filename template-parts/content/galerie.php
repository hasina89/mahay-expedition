<?php 

	$photos = get_field('photos');

	if(!empty($photos)) :

	    $galeries = $fancy_gal = array();

	    $i = 0;

	    $k = false;

	    foreach($photos as $p){

	    	if($i==1 || $i==4 || $i==7){

	    		$k = true;

	    		$g = array();

	    		$g[] = $p;

	    	}elseif($i ==2 || $i==5 || $i==8){

	    		$g[] = $p;

	    		$galeries[] = $g;

	    		$k = false;

	    		$g = array();

	    	}else{

	    		$galeries[] = $p;	    		

	    	}

	        $i++;

	    } 

?>



	<div class="listGalerie" id="listGalerie">

		<?php 

			foreach($galeries as $gal) : 

				if(count($gal) % 4) : ?>

					<div class="item item-2">

						<?php $k=1; foreach($gal as $g) :

									$img = !empty($g['sizes']['gal_thumb']) ? $g['sizes']['gal_thumb'] : $g['url']; 
									$fancy = !empty($g['sizes']['gal_fancy']) ? $g['sizes']['gal_fancy'] : $g['url']; 
							?>

                            <div class="content content-<?php echo $k; ?>">

                                <a href="<?php echo $fancy; ?>" title="<?php bloginfo('name') ?>" id="galerie-<?php echo $i; ?>" class="fancybox" data-fancybox="groupe1" rel="1">

                                    <img src="<?php echo $img; ?>" alt="<?php bloginfo('name') ?>">

                                </a>

                            </div>

                        <?php $k++; endforeach; ?>

					</div>

		<?php	else : $image = !empty($gal['sizes']['gal_large']) ? $gal['sizes']['gal_large'] : $gal['url']; ?>

					<div class="item">

						<div class="content">

			                <a href="<?php echo $image; ?>" title="<?php bloginfo('name') ?>" id="galerie-<?php echo $i; ?>" class="fancybox" data-fancybox="groupe1" rel="1">

			                    <img src="<?php echo $image; ?>" alt="<?php bloginfo('name') ?>">

			                </a>

			            </div>

					</div>

		<?php 	endif; 

			endforeach;

		?>

	</div>	

    <?php $i=1; foreach($fancy_gal as $f) : ?>

    	<a href="<?php echo $f ?>" title="<?php bloginfo('name') ?>" id="galerie-zoom-<?php echo $i; ?>" class="fancybox" data-fancybox="groupe1" rel="1"></a>

    <?php $i++; endforeach; 

endif; ?>