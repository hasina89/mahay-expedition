<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
	<style>

		.pageCircuitD .circuitInfo .blcRight,
		.pageCircuitD .circuitInfo .blcLeft 		{ z-index: 999; }
		.par_cols::after 							{ display: table; content: ""; clear: both; }
		.par_cols .col_left, .par_cols .col_right	{ width: 50%; float: left; }
		.par_cols .col_right img 					{ max-width: 100%; display: block; position: relative; margin-top: -7.3em; }
		.pageCircuitD .textParcours.with-map 		{ margin-bottom: 60px }
		.goog-te-gadget 							{ font-size: 0px !important; line-height: 0 !important; }
		#flags 										{ width: auto; vertical-align: middle; display: inline-block; }
		.blcCoord .language 						{ display: inline-block; }
		.blcCoord .language:after 					{ display: none; }
		#flags.size18 a.united-states[title="English"],
		a.single-language.united-states[title="English"] span.size18 { background: url('/wp-content/themes/mahay-expedition/images/en.png') center no-repeat; }
		#flags.size18 a[title="French"], 
		.tool-container .tool-items a[title="French"],
		 a.single-language[title="French"] span.size18 				{ background: url('/wp-content/themes/mahay-expedition/images/fr.png') center no-repeat; }
		 #flags li 													{ width: 30px; display: inline-block !important; height: auto !important; }

		@media screen and  (max-width: 768px)		{
			.pageCircuitD .circuitInfo .item 		{ padding-bottom: 40px }
			.par_cols .col_right img 				{ display: block; position: relative; margin-top: -2em; }
			.blcTestimonial 						{ background: #eeeeee; }
		}
		@media screen and  (max-width: 767px)			{
			.blcTestimonial .arrowTestimonial 			{ padding-bottom: 4em; }
		}
		@media (max-width: 600px){
			.blcCoord .tel 								{ margin-right: 0; }
			#flags li 									{ width: 25px; line-height: 64px; display: inline-block !important; padding-right: 5em !important; float: none !important; } 
			.blcCoord .language 						{ display: inline-block; height: 50px; padding-right: 5px}
			.wrap-avis .link 							{ border: 1px solid #e24540;}
			.wrap-avis 									{ padding-top: 2rem !important; }
			.blcTestimonial .arrowTestimonial 			{ padding-bottom: 6em; }
		}
		@media screen and  (max-width: 699px)			{
			.par_cols .col_left, .par_cols .col_right 	{ width: 100%; float: none; }
			.par_cols .col_right img  					{ margin-top: 0; }
		}
	</style>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	 
	  ga('create', 'UA-48504116-1', 'auto');
	  ga('send', 'pageview');
	</script>
</head>

<body <?php body_class(); ?>>
<div id="wrapper">	
	<header id="header" class="<?php echo is_front_page() ? 'home' : 'page'; ?>">
		<div class="headerTop">
            <?php get_template_part('template-parts/header/entry-header'); ?>
            <div class="wrapper">
                <div class="wrapMenuMobile"><div class="menuMobile"><div></div><?php _e('MENU','mahay_expedition') ?></div></div>
                <div class="blcMenu clr">
                	<?php 
                		wp_nav_menu(
							array(
								'theme_location' => 'principal',
								'menu_class'     => 'main-menu',
								'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								'container'      => 'nav',
								'container_class'=> 'menu',
								// 'depth' => 0,
							)
						);
						get_template_part('template-parts/header/site-branding');
                	?>
                </div>
            </div>
        </div>
		<?php get_template_part( 'template-parts/header/header-banner' ); ?>
	</header><!-- #header -->
	<main id="<?php echo is_front_page() ? 'homepage' : 'page' ?>" <?php post_class(); ?>>