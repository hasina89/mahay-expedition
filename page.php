<?php get_header(); ?>

	<section id="primary" class="content-area">
		<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content/content', 'page' );

			endwhile; 
		?>
	</section><!-- #primary -->

<?php get_footer(); ?>