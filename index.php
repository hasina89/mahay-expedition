<?php get_header(); wp_enqueue_script('equalheights'); ?>

	<div class="blcIntro" style="padding-bottom: 0; max-width: inherit;">
	    <div class="listesActu wrapper wow fadeIn" data-wow-delay="1800ms">
	        <div class="slideActu" id="slideActu">
	        	<?php 
	        		if(have_posts()) {

	        			while(have_posts()){
	        				the_post();
	        				get_template_part('template-parts/content/item', 'actus');
	        			} 

	        		}
	         	?>
	        </div>
	    </div>
	</div>	

<?php get_footer(); ?>