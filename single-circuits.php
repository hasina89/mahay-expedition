<?php get_header();?>

	<section id="primary" class="content-area">
		<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content/single', 'circuits' );

			endwhile; // End of the loop.
		?>
	</section><!-- #primary -->
	<script type="text/javascript">
		jQuery(document).ready(function($){
			$(".parcours").click(function() {
				$('html, body').animate({ scrollTop: $('.textParcours').offset().top }, 1000, "linear");
				return false;
			});
		})
	</script>
<?php get_footer(); ?>
