<?php 
    /* Template name: Page Home */
    get_header();
    $temoignages = get_field('temoignages'); 
    $news_titre = get_field('news_titre'); 
    $news_description = get_field('news_description'); 
    $lien_tr = get_field('lien_tr');
    $lien_qs = get_field('lien_qs');

    $lien_tr_url = !empty($lien_tr['url']) ? $lien_tr['url'] : '#';
    $lien_qs_url = !empty($lien_qs['url']) ? $lien_qs['url'] : '#';
    $lien_tr_txt = !empty($lien_tr['title']) ? $lien_tr['title'] : __('Qui sommes-nous ?', 'mahay_expedition');
    $lien_qs_txt = !empty($lien_qs['title']) ? $lien_qs['title'] : __('Tourisme durable', 'mahay_expedition');
    $args = array(
        'post__in'  => get_option( 'sticky_posts' ),
        'post_status' => 'publish',
        'posts_per_page' => 2,
        'orderby' => 'date',
        'order' => 'desc',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field'    => 'slug',
                'terms'    => array('actualites'),
            ),
        ),
    );
    $loop = new WP_Query($args);
    $themes = get_terms( 'themes', array('orderby' => 'id','hide_empty'=> false) );
    $tpm_2_1 = $themes[0];
    $tpm_3_1 = $themes[0];
    $tpm_3_2 = $themes[1];
    $themes_2 = array_slice($themes, 1);
    $themes_3 = array_slice($themes, 2);
    $themes_2[] = $tpm_2_1;
    $themes_3[] = $tpm_3_1;
    $themes_3[] = $tpm_3_2;

    if(!empty($themes)) : 
?>
    <style> 
        .blcTestimonial{
            overflow: hidden;
        }
        .wrap-avis{
            position: absolute;
            left: 0;
            width: 100%;
            text-align: right;
            top: 0;
            padding-top: 133px;
            margin-top: -3px;
        }
        .wrap-avis .link{
            position: absolute;
            text-align: center;
        }
        @media screen and (max-width: 1200px){
            .wrap-avis{
                position: relative;
                background: #c3312d
            }
            .wrap-avis .link{
                width: 100% !important
            }
        }
        @media screen and (max-width: 1539px){
            .wrap-avis{ 
                text-align: center;
                padding-top: 0;
                margin-top: 0;
                top: auto;
                bottom: 0;
            }
            .wrap-avis .link {
                position: relative;
                text-align: center;
                bottom: 0;
                width: auto !important;
            }
        }
        @media screen and (max-width: 1049px){
            .blcTestimonial .blcPetitFute .blcGuide2 { left: 172px; width: 173px; background-size: contain; }
        }
        @media screen and (max-width: 979px){
            .blcTestimonial .blcPetitFute {display: none; }
        }
    </style>
    <div class="blcService" id="blcService">
        <div class="wrapper clr">
            <div class="listService clr wow fadeIn" data-wow-delay="800ms" id="listService">
                <?php 
                    foreach($themes as $t) {
                        global $t;
                        include(locate_template('./template-parts/content/item-themes.php'));
                    } 
                ?>
            </div>
            <div class="listService clr wow fadeIn" data-wow-delay="800ms" id="listService-2">
                <?php 
                    foreach($themes_2 as $t) {
                        global $t;
                        include(locate_template('./template-parts/content/item-themes.php'));
                    } 
                ?>
            </div>
            <div class="listService clr wow fadeIn" data-wow-delay="800ms" id="listService-3">
                <?php 
                    foreach($themes_3 as $t) {
                        global $t;
                        include(locate_template('./template-parts/content/item-themes.php'));
                    } 
                ?>
            </div>
            <button type="button" id="prevSlide" class="slick-prev"></button>
            <button type="button"  id="nextSlide" class="slick-next"></button>
            <a href="#blcBienvenue" title="<?php _e('Notre éthique','mahay_expedition') ?>" class="ethique"><?php _e('Notre éthique','mahay_expedition') ?></a>
        </div>
    </div>
    <?php endif; ?>

    <div id="blcBienvenue" class="blcBienvenue wow fadeIn" data-wow-delay="800ms">
        <div class="content">
            <h1 class="titre"><?php the_field('titre') ?> <br> <span class="s-titre"><?php the_field('sous_titre') ?></span></h1>
            <!-- <h3 class="s-titre"><?php the_field('sous_titre') ?></h3> -->
            <div class="entr-content">
                <?php the_field('contenu') ?>
            </div>
            <div class="blcLink">
                <a href="<?php echo esc_url($lien_qs_url) ?>" class="link" title="<?php echo $lien_qs_txt ?>"><?php echo $lien_qs_txt ?></a>
                <!-- <a href="<?php echo esc_url($lien_tr_url) ?>" class="link" title="<?php echo $lien_tr_txt ?>"><?php echo $lien_tr_txt ?></a> -->
            </div>
        </div>
    </div>
    
    <div class="blcGalerie wow fadeIn" data-wow-delay="1000ms">
        <div class="wrapper">
            <?php get_template_part('template-parts/content/galerie') ?>
        </div>
    </div>

    <div class="blcAtout">
        <div class="content">
            <h2 class="titre">Pourquoi voyager avec nous ? </h2>
            <div class="lstAtout">
                <div class="item">
                    <div class="inner">
                        <div class="icon">
                            <img src="images/tourisme.png" alt="Tourisme Eco-responsable">
                        </div>
                        <div class="titre">Tourisme Eco-responsable</div>
                        <div class="txt">
                            <p>Reconnue à l'international, notre agence applique les critères du Tourisme Durable et Responsable.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if($loop->have_posts()) : ?>
    <div class="blcActu wow fadeIn" data-wow-delay="800ms">
        <div class="content">
            <div class="blcTitre clr">
                <h2 class="titre"><?php echo !empty($news_titre) ? $news_titre : __('Dernières actualités', 'mahay_expedition') ?></h2>
                <a href="<?php echo get_the_permalink(14) ?>" title="<?php _e('Voir tout','mahay_expedition') ?>" class="link"><?php _e('Voir tout','mahay_expedition') ?></a>  
            </div>
            <div class="entry-content">
                <?php echo $news_description ?>
            </div>
            <div class="listActu" id="listActu">
                <?php while($loop->have_posts()): $loop->the_post(); global $post; ?>
                <div class="item">
                    <div class="container clr">
                        <div class="date">
                            <?php mahay_the_date() ?>
                        </div>
                        <div class="actu">
                            <a href="<?php echo get_the_permalink(); ?>" class="s-titre" title="<?php the_title() ?>"><?php the_title() ?></a>
                            <p><?php echo wp_trim_words(get_the_excerpt(), 17) ?></p>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>


    <?php 
        endif; wp_reset_query($loop); 
        if(!empty($temoignages)) : 
    ?>
    <div class="blcTestimonial">
        <div class="blcTitre">
            <h2 class="titre"><?php the_field('test_titre') ?></h2>
            <div class="arrowTestimonial"></div>
        </div>
        <div class="blcPetitFute">
            <a href="<?php echo esc_url(get_field('petit_fute')) ?>" target="_blank" title="<?php _e('Petit Futé', 'mahay_expedition') ?>" class="blcGuide"></a>
            <a href="<?php echo esc_url(get_field('babel_voyages')) ?>" target="_blank" title="<?php _e('Babel Voyages', 'mahay_expedition') ?>" class="blcGuide3"></a>
            <a href="<?php echo esc_url(get_field('quota_trip')) ?>" target="_blank" title="<?php _e('Quota Trip', 'mahay_expedition') ?>" class="blcGuide2"></a>
        </div>



        <div class="slideTestimonial" id="slideTestimonial">
            <?php $i=1; foreach($temoignages as $t) : 
                            $t_id = $t->ID;
                            $photo = get_field('photo', $t_id);
                            $bg = !empty($photo['sizes']['bg_testi']) ? $photo['sizes']['bg_testi'] : $photo['url'];
                            $bg = !empty($bg) ? $bg : get_theme_file_uri('images/bg-testimonial-2.jpg');
            ?>
                <div class="item item-<?php echo $i; ?>">
                    <div class="bg" style="background-image: url('<?php echo $bg ?>')">
                        <div class="wrapper clr">
                            <div class="content wow fadeInUp" data-wow-delay="800ms">
                                <div class="blcName">
                                    <span><?php the_field('titre', $t_id) ?></span>
                                    <span class="name"><?php echo $t->post_title ?></span>
                                </div>
                                <div class="blcText">
                                    <p><?php echo wp_trim_words(strip_tags(get_field('description', $t_id)), 40); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            <?php $i++; endforeach; ?>
        </div>
        <div class="wrap-avis"> 
            <div class="wrapper">   
                <a href="<?php echo home_url('/contact/?objet=avis') ?>" title="<?php _e('Donnez votre avis', 'mahay_expedition') ?>" class="link"><?php _e('Donnez votre avis', 'mahay_expedition') ?></a>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function update_size_avis(){
            var w = jQuery(window).width();
            var ww = jQuery('.wrap-avis .wrapper').innerWidth();
            var pr = parseInt(jQuery('.wrap-avis .wrapper').css('padding-right'));
            pr = pr*2;
            ww = ww - pr;
            var wa = (w-ww)/2;
            jQuery('.wrap-avis .link').css('width', wa);
        }
        jQuery(document).ready(function(){ update_size_avis(); });
        jQuery(window).resize(function(){ update_size_avis(); });
    </script>
    <?php endif;  
get_footer() ?>