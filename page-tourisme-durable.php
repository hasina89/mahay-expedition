<?php 
    /* Template name: Page Tourisme Durable */
    get_header(); 
       
?>
<div class="pageCircuitL">
    <div class="blcIntro">
        <h1 class="titre wow fadeInUp" data-wow-delay="600ms"><?php the_field('tax_titre') ?> <span class="mahay-2"><?php the_field('tax_stitre') ?></span></h1>
        <div class="wow fadeInUp" data-wow-delay="1200ms">
            <?php the_field('tax_contenu') ?>
        </div>  
    </div>
    
    <div class="wrapper">
        <div class="content-circuit">
            <div class="listeCircuit clr">
                <?php 
                    $onglets = get_field('bloc_de_liens'); 
                    if( is_array( $onglets ) ):
                        foreach( $onglets as $onglet ):
                ?>
                        <div class="item wow fadeInUp" dataa-wow-delay="<?php echo $time ?>ms">
                            <div class="content">
                                <div class="blcTitre">
                                    <a href="<?php echo $onglet['lien']; ?>" class="s-titre" title="<?php echo $onglet['titre'] ?>"><?php echo $onglet['titre'] ?></a>
                                </div>
                                <div class="wow fadeInUp" data-wow-delay="1000ms">
                                   <?php echo $onglet['description'] ?>
                                </div>
                                <div class="blcTarif clr">
                                    <a href="<?php echo $onglet['lien']; ?>" class="link" title="<?php _e('Découvrir','mahay_expedition') ?>"><?php _e('Découvrir','mahay_expedition') ?></a>
                                </div>
                            </div>
                            <div class="img">
                                <img src="<?php echo $onglet['image_onglet'] ?>" alt="<?php echo $onglet['titre'] ?>">
                            </div>
                        </div>
                        <?php $time += 400; ?>
                    <?php endforeach; endif; ?>
           
                </div>
        </div>   
    </div>
    
</div>
<?php get_footer(); ?>