<?php 
	/* Template name: Page Toursime */  
	get_header(); 
    $types = get_terms( 'groups', array('orderby' => 'id','hide_empty'=> false) );
?>
<div class="wrapper">
    <div class="blcIntro">
        <?php mahay_page_title() ;
        if(have_posts()) : while(have_posts()) : the_post(); ?>
        <div class="wow fadeInUp" data-wow-delay="1200ms">
            <?php the_content() ?>
        </div>
        <?php endwhile; endif; ?>
    </div>
    <div class="blcGalerie wow fadeIn" data-wow-delay="1800ms">
        <?php get_template_part('template-parts/content/galerie') ?>
    </div>
</div>
<div class="blcPartenaire">
    <div class="blcText">
        <h2 class="titre wow fadeInUp" data-wow-delay="400ms"><?php the_field('par_ttitre') ?> <span><?php the_field('par_stitre') ?></span></h2>
        <div class="wow fadeInUp" data-wow-delay="1000ms">
           <?php the_field('contenu') ?>
        </div>
    </div>
    <div class="filtrePartenaire">
        <ul class="clr tabs">
            <?php $i=1; foreach($types as $t) : ?>
            <li class="wow fadeInUp <?php echo $i==1 ? 'active' : ''; ?>" data-wow-delay="1200ms"><span data-item="#tab<?php echo $i; ?>" title="<?php echo $t->name ?>"><?php echo $t->name ?></span></li>
            <?php $i++; endforeach; ?>
        </ul>
    </div>
    <div class="listPartenaire tab_container wow fadeIn" data-wow-delay="2800ms">
        <?php 
            $tab1 = get_query_partenaire(array(20));    
            if($tab1->have_posts()):
        ?>
            <div class="tab_content assoc" id="tab1">
                <div class="listItem sliderPartenaire clr">
                    <?php 
                        while($tab1->have_posts()){
                            $tab1->the_post();
                            get_template_part('template-parts/content/item','partenaire');
                        }
                    ?>
                </div>
            </div>
        <?php endif; 
            $tab2 = get_query_partenaire(array(21));    
            if($tab2->have_posts()):
        ?>
         <div class="tab_content ong" id="tab2">
            <div class="listItem sliderPartenaire clr">
                <?php 
                    while($tab2->have_posts()){
                        $tab2->the_post();
                        get_template_part('template-parts/content/item','partenaire');
                    }
                ?>
            </div>
        </div>
        <?php endif; 
            $tab3 = get_query_partenaire(array(22));    
            if($tab3->have_posts()):
        ?>
        <div class="tab_content artisanats" id="tab3">
            <div class="listItem sliderPartenaire clr">
                <?php 
                    while($tab3->have_posts()){
                        $tab3->the_post();
                        get_template_part('template-parts/content/item','partenaire');
                    }
                ?>                           
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($) {  
          $('.tab_content').addClass("hide");
          $('.tab_content:first').removeClass("hide");
          $('.tabs li:first').addClass('active');
          $('.tabs li').click(function(event) {
            $('.tabs li').removeClass('active');
            $('.tab_content').addClass("hide");
            $(this).addClass('active');
            var selectTab = $(this).find('span').attr("data-item");
            $(selectTab).removeClass("hide");

            
          });
    })
</script>     
<?php get_footer(); ?>