</main><!-- #content -->

	<!-- FOOTER START -->
	<footer id="footer" <?php if ($namePage != "pageAccueil") echo("class='page'"); ?>>
        <div class="footer">
          	<div class="wrapper clr">
          		<?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?> 
          	</div>
          	<div class="blcBottom clr">
          		<div class="blcMention">
          			<?php dynamic_sidebar('mentions'); ?>
          		</div>
          		<div class="blcMaki">
          			<?php _e('Réalisé par', 'mahay_expedition') ?> <a href="http://maki-agency.mg/" title="<?php _e('Réalisé par Maki Agency', 'mahay_expedition') ?>" target="_blank">Maki Agency</a>
          		</div>          		
          	</div>
        </div>
 	</footer><!-- #footer -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>