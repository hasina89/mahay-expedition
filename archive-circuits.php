<?php 
	get_header();
	$args = array(
        'post_type' => 'circuits',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'desc'
    );
    $result = new WP_Query($args);


	$data = array_map(
		function( $post ) {
			return (array) $post;
		},
		$result->posts
	);
	$circuits = array_chunk($data, 3);
?>

	<div class="pageCircuitL">
		<div class="blcIntro">
		    <h1 class="titre wow fadeInUp" data-wow-delay="600ms"><?php _e('Nos circuits',  'mahay_expedition') ?></h1>
		</div>
		<div class="wrapper">
	        <div class="content-circuit">
	            <?php $time = $start = 1600; $i=0; foreach($circuits as $circuit) : ?>
	            	<div class="listeCircuit clr <?php echo $i==0 ? 'border' : '' ?>">
	            		<?php 
	            			
	            			if(is_array($circuit)) : foreach($circuit as $c) : 
	            				$title = get_the_title($c['ID']);
	            				$link = get_the_permalink($c['ID']);
	            				$excerpt = get_field('resumer', $c['ID']);
	            				$prix = get_field('prix', $c['ID']);
	            				$duree = get_field('duree', $c['ID']);
	            				$thumb = get_field('post_humbnail', $c['ID']);
	            				$img = !empty($thumb['sizes']['actus_img']) ? $thumb['sizes']['actus_img'] : get_theme_file_uri('images/img-circuitl-1.jpg');
	            				$time = $i > 0 ? $start/4 : $time;
	            				$start = $i > 0 ? $start/4 : $start;
	            		?>
	            			<div class="item wow fadeInUp" data-wow-delay="<?php echo $time ?>ms" data-pid="<?php echo $c['ID']; ?>">
	                            <div class="content">
	                                <div class="blcTitre">
	                                    <div class="jour">
	                                        <span><?php echo strip_tags($duree['d_jours']) ?><em><?php _e('j', 'mahay_expedition') ?></em></span><span><?php echo strip_tags($duree['d_nuits']) ?><em><?php _e('n', 'mahay_expedition') ?></em></span>
	                                    </div>
	                                    <a href="<?php echo $link; ?>" class="s-titre" title="<?php echo $title ?>"><?php echo $title ?></a>
	                                    <span><?php echo strip_tags($excerpt) ?></span>
	                                </div>
	                                <div class="blcTarif clr">
	                                    <div class="left">
	                                        <div><?php _e('A partir de', 'mahay_expedition') ?>  </div>
	                                        <div><span><b><?php echo strip_tags($prix) ?><sup>€</sup></b></span>/ <?php _e('personne', 'mahay_expedition') ?></div>
	                                    </div>
	                                    <a href="<?php echo $link; ?>" class="link" title="<?php _e('Découvrir','mahay_expedition') ?>"><?php _e('Découvrir','mahay_expedition') ?></a>
	                                </div>
	                            </div>
	                            <div class="img">
	                                <img src="<?php echo $img ?>" alt="<?php echo $title ?>">
	                            </div>
	                        </div>
	            		<?php $time += 400; endforeach; endif; ?>
	            	</div>
	            <?php $i++; endforeach; ?>
	        </div>   
	    </div>
	</div>

<?php get_footer(); ?>