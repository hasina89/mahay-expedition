<?php 
	/* Template name: Page Toursime sans partenaire */  
	get_header(); 
    $types = get_terms( 'groups', array('orderby' => 'id','hide_empty'=> false) );
?>
<div class="wrapper">
    <div class="blcIntro">
        <?php mahay_page_title() ;
        if(have_posts()) : while(have_posts()) : the_post(); ?>
        <div class="wow fadeInUp" data-wow-delay="1200ms">
            <?php the_content() ?>
        </div>
        <?php endwhile; endif; ?>
    </div>
    <div class="blcGalerie wow fadeIn" data-wow-delay="1800ms">
        <?php get_template_part('template-parts/content/galerie') ?>
    </div>
</div>
     
<?php get_footer(); ?>