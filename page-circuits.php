<?php 
	/* Template name: Page Circuits */  
	get_header(); 
	wp_enqueue_script('equalheights');
	$order = get_field('ordre');
	$nbr = get_field('nbr');
	$args = array(
		'post_type' => 'circuits',
		'post_status' =>  'publish',
		'posts_per_page' => $nbr,
		'orderby' => 'date',
		'order' => "{$order}"
	);
	$d_args = array(
	'post_type' => 'post',
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => array( 'depart' ),
			),
		),
	);
	$loop = new WP_Query($args);
	$d_loop = new WP_Query($d_args);
	$video_youtube = get_field('video_youtube');
	$v_url = !empty($video_youtube['url']) ? $video_youtube['url'] : 'https://www.youtube.com/embed/-h_7n2Mkvaw';
	$v_title = !empty($video_youtube['title']) ? $video_youtube['title'] : __('Madagascar Touring en action, voir la vidéo');
?>
	<div class="Circuit">
		<div class="wrapper">
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
				<div class="blocContent wow fadeIn" data-wow-delay="900ms">
					<?php the_content() ?>
				</div><!-- #blocContent -->
			<?php endwhile; endif; ?>
				<div class="container">
					<div class="blcFiltre wow fadeIn" data-wow-delay="900ms" id="filtre">
			        	<div class="content">
							<?php get_search_form(); ?>
						</div>
					</div><!-- #blcFiltre -->
					<?php if($loop->have_posts()) : ?>
					<div class="lst-actu-page clr">
						<?php 
							$i=0;
							while($loop->have_posts()) : 
								$loop->the_post(); 
									include (locate_template( 'template-parts/content/content-single-circuits.php' ));
								$i++;
							endwhile; 
							wp_reset_query($loop);
						?>
					</div><!-- #lst-actu-page -->
					<?php endif; ?>
				</div><!-- #container -->
		</div><!-- #wrapper -->		
		<div class="blcLnkVideo wow fadeInUp" data-wow-delay="1000ms">
			<a class="link fancybox various" href="https://www.youtube.com/embed/-h_7n2Mkvaw" title="<?php echo $v_title ?>"><?php echo $v_title ?></a>
		</div><!-- #blcLnkVideo -->
		<?php if($d_loop->have_posts()) :  ?>
		<div class="blcDepart">
			<div class="wrapper">
				<h2 class="wow fadeInUp" data-wow-delay="1200ms"><?php the_field('d_titre') ?></h2>
				<div class="wow fadeInUp" data-wow-delay="1200ms"><?php the_field('d_contenu') ?></div>
				<div class="blcSlide wow fadeInUp" data-wow-delay="1200ms">
					<div class="listItem" id="departSlide">
						<?php 
							while($d_loop->have_posts()) : 
								$d_loop->the_post(); 
								global $post; 
								$img = get_field('image');
								$img = !empty($img) ? !empty($img['sizes']['circuit_img']) ? $img['sizes']['circuit_img'] : $img['url'] : get_theme_file_uri('images/slidecircuit1.jpg');

						?>
						<div class="item">
							<div class="blcImage">
								<img src="<?php echo $img; ?>" alt="<?php bloginfo('name'); ?>">
							</div>
							<div class="blcText">
								<h3><?php echo $post->post_title ?></h3>
								<p><?php the_field('d_date'); ?> <br><?php _e('Place limité à', 'mada_touring'); echo '&nbsp;'; the_field('d_place'); echo '&nbsp;'; _e('personnes', 'mada_touring') ?></p>
								<p><?php _e('Il reste', 'mada_touring') ?> <span><?php the_field('d_dispo'); echo '&nbsp;'; _e('places', 'mada_touring'); ?></span> <?php _e('disponible', 'mada_touring') ?></p>
								<a class="link" href="<?php echo get_the_permalink($post->ID); ?>" title="<?php _e('Voir le programme','mada_touring') ?>"><?php _e('Voir le programme','mada_touring') ?></a>
							</div>
						</div>
						<?php endwhile; wp_reset_query($d_loop); ?>
					</div>
					<div class="arwDepart"></div>
				</div>
			</div>
		</div><!-- #blcDepart -->
		<?php endif; ?>
	</div>
	<script type="text/javascript">
		jQuery(document).ready(function($){
		 	$('#departSlide').slick({
		        dots:false,
		        infinite:true,
		        autoplaySpeed:4000,
		        speed:980,
		        fade:true,
		        arrows:true,
		        autoplay:true,
		        pauseOnHover:false,
		        autoplay:true,
		        appendArrows: $('.arwDepart'),
		        responsive: [
					{ 
					  breakpoint: 767,
					  settings: {
					  	 adaptiveHeight: true
					  } 
					},  
				 ]
		    });
		    $('.lst-actu-page .item').equalHeights();
		});
	</script>
<?php get_footer(); ?>