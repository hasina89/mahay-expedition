<?php 
/**
 * Extend Recent Posts Widget 
 *
 * Adds different formatting to the default WordPress Recent Posts Widget
 */

Class MADA_Recent_Posts_Widget extends WP_Widget_Recent_Posts {

	function widget($args, $instance) {
	
		extract( $args );
		
		$title = apply_filters('widget_title', empty($instance['title']) ? __('Recent Posts') : $instance['title'], $instance, $this->id_base);
				
		if( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) )
			$number = 10;
					
		$r = new WP_Query( apply_filters( 'widget_posts_args', array( 'posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true, 'post__not_in'=>array(get_the_ID()) ) ) );
		if( $r->have_posts() ) :
			
			echo $before_widget;
			if( $title ) echo $before_title . $title . $after_title; ?>
			<div class="lastActu">
				<?php while( $r->have_posts() ) : $r->the_post(); $link = get_the_permalink($r->ID); $title = get_the_title( $r->ID ); $excerpt = wp_trim_words( get_the_excerpt(), 17, "..." );?>
                    <div class="item">
                        <div class="container clr">
                            <div class="date">
                                <?php mahay_the_date() ?>
                            </div>
                            <div class="actu">
                                <a href="<?php echo $link ?>" class="s-titre" title="<?php echo $title ?>"><?php echo $title ?></a>
                                <p><?php echo $excerpt ?></p>
                            </div>
                        </div>
                    </div>
                   <?php endwhile; ?>
            </div>
			 
			<?php
			echo $after_widget;
		
		wp_reset_postdata();
		
		endif;
	}
}

/**
 * Register widgets
*/
function mada_touring_recent_widget_registration() {
  unregister_widget('WP_Widget_Recent_Posts');
  register_widget('MADA_Recent_Posts_Widget');
}
add_action('widgets_init', 'mada_touring_recent_widget_registration');