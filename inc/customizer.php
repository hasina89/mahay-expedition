<?php
function mada_customize_register( $wp_customize ) { 

    /** 
     * Add new panel
    */
    $wp_customize->add_panel( 'mada_options', array(
        'title' => __('Thème options', 'motelanosy'),
        'priority' => 300,
    ) );

    /** 
     * Options : Header
    */
    $wp_customize->add_section(
        'mada_section_header',
        array(
            'title' => __( 'Logo', 'maha_expedition' ),
            'priority' => 200,
            'capability' => 'edit_theme_options',
            'description' => '',
            'panel' => 'mada_options',
        )
    );    
    // Controls
    $wp_customize->add_setting( 'header_logo', array( 'default' => get_theme_file_uri( 'images/logo.png') ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_logo', array(
            'label' => __( 'Logo', 'maha_expedition' ),
            'section' => 'mada_section_header',
            'settings' => 'header_logo',
            )
        )
    );
    /*$wp_customize->add_setting( 'header_logo_m', array( 'default' => get_theme_file_uri( 'images/logo-mob2.png') ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_logo_m', array(
            'label' => __( 'Logo Mobile', 'maha_expedition' ),
            'section' => 'mada_section_header',
            'settings' => 'header_logo_m',
            )
        )
    );*/
    $wp_customize->add_setting( 'header_logo_f', array( 'default' => get_theme_file_uri( 'images/logo-f.png') ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_logo_f', array(
            'label' => __( 'Logo Footer', 'maha_expedition' ),
            'section' => 'mada_section_header',
            'settings' => 'header_logo_f',
            )
        )
    );

    /** 
     * Options : Coordonnées
    */
    $wp_customize->add_section(
        'mada_opt_coords',
        array(
            'title' => __( 'Options', 'maha_expedition' ),
            'priority' => 205,
            'capability' => 'edit_theme_options',
            'description' => '',
            'panel' => 'mada_options',
        )
    );
    // controls
    $wp_customize->add_setting( 'opts_slogan', array( 'default' => 'Tour operator - location de voiture' ) );
    $wp_customize->add_setting( 'opts_phone', array( 'default' => '+261 34 15 441 23' ) );
    $wp_customize->add_setting( 'opts_email', array( 'default' => 'contact@mahayexpedition.com' ) );
    $wp_customize->add_setting( 'opts_copy', array( 'default' => '© Copyright Mahay Expédition 2019' ) );
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'opts_slogan', array(
            'label' => __( 'Slogan', 'maha_expedition' ),
            'section' => 'mada_opt_coords',
            'settings' => 'opts_slogan',
            'type' => 'text'
            )
        )
    );
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'opts_phone', array(
            'label' => __( 'Téléphone', 'maha_expedition' ),
            'section' => 'mada_opt_coords',
            'settings' => 'opts_phone',
            'type' => 'text'
            )
        )
    );
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'opts_email', array(
            'label' => __( 'Email', 'maha_expedition' ),
            'section' => 'mada_opt_coords',
            'settings' => 'opts_email',
            'type' => 'email'
            )
        )
    );
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'opts_copy', array(
            'label' => __( 'Copyright', 'maha_expedition' ),
            'section' => 'mada_opt_coords',
            'settings' => 'opts_copy',
            'type' => 'text'
            )
        )
    );
    
    /** 
     * Options : Coordonnées
    */
    $wp_customize->add_section(
        'mada_opt_sociaux',
        array(
            'title' => __( 'Réseaux sociaux', 'maha_expedition' ),
            'priority' => 204,
            'capability' => 'edit_theme_options',
            'description' => '',
            'panel' => 'mada_options',
        )
    );
    // controls
    $wp_customize->add_setting( 'sociale_fb', array( 'default' => 'https://www.facebook.com/MahayExpedition' ) );
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'sociale_fb', array(
            'label' => __( 'Facebook', 'maha_expedition' ),
            'section' => 'mada_opt_sociaux',
            'settings' => 'sociale_fb',
            'type' => 'text'
            )
        )
    );
    $wp_customize->add_setting( 'sociale_gp', array( 'default' => 'https://plus.google.com/+Stéphanethamin' ) );
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'sociale_gp', array(
            'label' => __( 'Google plus', 'maha_expedition' ),
            'section' => 'mada_opt_sociaux',
            'settings' => 'sociale_gp',
            'type' => 'text'
            )
        )
    );
    $wp_customize->add_setting( 'sociale_tw', array( 'default' => 'https://twitter.com/MahayExpedition' ) );
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'sociale_tw', array(
            'label' => __( 'Twitter', 'maha_expedition' ),
            'section' => 'mada_opt_sociaux',
            'settings' => 'sociale_tw',
            'type' => 'text'
            )
        )
    );

}
add_action( 'customize_register', 'mada_customize_register',10,2 );