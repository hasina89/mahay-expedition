<?php

/* Body classes */
if( !function_exists('mahay_body_classes') ) : 

	function mahay_body_classes( $classes ) {

		if ( is_singular() ) {
			$classes[] = 'singular';
		} else {
			$classes[] = 'pages';
		}
		if ( mahay_image_filters_enabled() ) {
			$classes[] = 'image-filters-enabled';
		}
		return $classes;
	}

endif;
add_filter( 'body_class', 'mahay_body_classes' );

/* Posts classes */
if( !function_exists(' mahay_post_classes') ) :

	function mahay_post_classes( $classes, $class, $post_id ) {

		switch ($post_id) {
			case 8:
			case 1282:
				$classes[] = 'pageAboutUs';
				break;
			case 16:
			case 1275:
				$classes[] = 'pageContact';
				break;
			case 12:
			case 1277:
				$classes[] = 'pageMada';
				break;
			case 138:
			case 1272:
				$classes[] = 'circuit';
				break;
			case 14:
			case 1132:
				$classes[] = 'PageActuL';
				break;
			case 178:
			case 1132:
				$classes[] = 'pagePartenaires';
				break;
			case 254:
			case 1132:
				$classes[] = 'pageTourisme';
				break;
			
			default:
				$classes[] = 'entry';
				break;
		}

		if ( is_home() || is_category() ) {
			$classes[] = 'PageActuL';
		}elseif( is_singular('post') ){
			$classes[] = 'pageActuD';
		}elseif( is_singular('circuits') ){
			$classes[] = 'pageCircuitD';
		}elseif( is_singular('partenaires') ){
			$classes[] = 'pagePartenaires';
		}

		return $classes;
	}

endif;
add_filter( 'post_class', 'mahay_post_classes', 10, 3 );

if( !function_exists('translation_recaptcha') ) :

	function translation_recaptcha() {
		wp_deregister_script( 'google-recaptcha' );
		$code = ICL_LANGUAGE_CODE == 'fr' ? 'fr' : 'en';
		$url = 'https://www.google.com/recaptcha/api.js';
		$url = add_query_arg( array(
			'onload' => 'recaptchaCallback',
			'render' => 'explicit',
		 	'hl' => $code ), $url );

		wp_register_script( 'google-recaptcha', $url, array(), '2.0', true );
	}

endif;
add_action( 'wpcf7_enqueue_scripts', 'translation_recaptcha', 11 );

/**
* Header Banner Class Dynamic
*/

if( !function_exists('banner_classe') ):

	function banner_classe(){
		switch (get_the_ID()) {
			case 8:
				$class = 'qui-sommes-nous';
				break;
			case 10:
				$class = 'circuitL';
				break;
			case 12:
				$class = 'mada';
				break;
			case 14:
				$class = 'actuL';
				break;
			case 16:
				$class = 'contact';
				break;	
			case 178:
				$class = 'partenaires';
				break;
			case 254:
				$class = 'tourisme';
				break;			
			default:
				$class = 'default';
				if(is_home()){$class = 'actuL'; }
				elseif(is_singular('post')) { $class = 'actuD'; }
				elseif(is_category()) { $class = 'actuD'; }
				elseif(is_singular('partenaires')) { $class = 'partenaires'; }
				else { $class = 'circuitL'; }

				break;
		}
		echo $class;
	}

endif;

/**
* Menu Class Active
*/
if( !function_exists('mahay_nav_menu_class') ) :

	function mahay_nav_menu_class( $classes, $item, $args, $depth ) {
		if(in_array('current-menu-item', $item->classes)){
				$classes[] =  'active';
		}
		if( in_array('current_page_parent', $item->classes) ){
			if(is_singular('post')){
				$classes[] =  'active';
			}			
		}
		if(is_singular('circuits') && $item->ID == 29 || is_singular('circuits') && $item->ID == 434){
			$classes[] =  'active';
		}	
		if(is_post_type_archive('circuits') && $item->ID == 29 ){
			$classes[] =  'active';
		}
		if( in_array('current-menu-ancestor', $item->classes) ){
			if( is_tax('themes')){
				$classes[] =  'active';
			}
		}
		if( (is_category() && $item->ID == 27) || (is_singular('post') && $item->ID == 27) || ( is_singular('post') && $item->ID == 1287 )){
			$classes[] =  'active';
		}
		// print_r($item);
		return $classes;
	}	

endif;
add_filter( 'nav_menu_css_class', 'mahay_nav_menu_class', 10, 4 ); 

/**
* Sub Menu Class
*/

if( !function_exists('mahay_submenu_css_class') ):

function mahay_submenu_css_class( $classes ) {
	unset($classes);
    $classes[] = 'sub';
    return $classes;
}

endif;
add_filter( 'nav_menu_submenu_css_class', 'mahay_submenu_css_class' );

/**
 * Filter :: Change WP Mail Content
 * 
 */
function mahay_set_content_type(){
    return "text/html";
}
add_filter( 'wp_mail_content_type','mahay_set_content_type',10,2);

/**
 * Filter :: Remove Wordpress From E-mail
 * 
 */
function mahay_remove_from_wordpress($email){
    $wpfrom = get_option('blogname');
    return $wpfrom;
}
add_filter('wp_mail_from_name', 'mahay_remove_from_wordpress');

/**
 * Filter :: Remove Wordpress From E-mail Url
 * 
 */
function mahay_mail_from() { 
    return 'contact@mahayexpedition.com'; 
}
add_filter('wp_mail_from', 'mahay_mail_from');

/* Menu Items */
/*if( !function_exists('mahay_nav_items') ) :

	function mahay_nav_items($items, $args) {
		if($args->theme_location == "principal"){
			$menu = '<li class="logo-mob"><a href="'.home_url().'" title="'.get_bloginfo('name').'"><img src="'.get_theme_file_uri('images/logo-mob.jpg').'" alt="'.get_bloginfo('name').'"></a></li>';
			$items = explode("</li>",$items); 
			$i = 0;
			foreach($items as $item){
				if($i==9){
					$menu .= '<li class="white-space"></li>';
				}
				$menu .= $item.'</li>';
				$i++;
			}
		}
		return $menu;
	}	

endif;
add_filter( 'wp_nav_menu_items', 'mahay_nav_items', 10, 4 ); */

/* Add a pingback url */
if( !function_exists('mahay_pingback_header') ) : 

	function mahay_pingback_header() {
		if ( is_singular() && pings_open() ) {
			echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
		}
	}

endif;
add_action( 'wp_head', 'mahay_pingback_header' );


/* TinyMCE : enable span */
if( !function_exists('mahay_override_mce_options') ) :

	function mahay_override_mce_options($initArray){
		$opts = '*[*]';
		$initArray['valid_elements'] = $opts;
		$initArray['extended_valid_elements'] = $opts;
		return $initArray;
	}

endif;
add_filter('tiny_mce_before_init', 'mahay_override_mce_options');

/* Allow SVG to Media Uploader */
if( !function_exists('mahay_mime_types') ) : 

	function mahay_mime_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}

endif;
add_filter('upload_mimes', 'mahay_mime_types');

/* Excerpt length */
if( !function_exists('mahay_excerpt_length') ) :

	function mahay_excerpt_length( $length ) {
		return 18;
	}

endif;
add_filter( 'excerpt_length', 'mahay_excerpt_length', 999 );

/* Excerpt more */
if( !function_exists('mahay_excerpt_more') ):

	function mahay_excerpt_more($more) {
	   global $post;
	   return '';
	}

endif;
add_filter('excerpt_more', 'mahay_excerpt_more');

if( !function_exists('mahay_tinymce_term_description') ) :

	function mahay_tinymce_term_description($term, $taxonomy){
	    ?>
	    <tr valign="top">
	        <th scope="row">Description</th>
	        <td>
	            <?php wp_editor(html_entity_decode($term->description), 'description', array('media_buttons' => false)); ?>
	            <script>
	                jQuery(window).ready(function(){
	                    jQuery('label[for=description]').parent().parent().remove();
	                });
	            </script>
	        </td>
	    </tr>
	    <?php
	} 

endif;
add_action("themes_edit_form_fields", 'mahay_tinymce_term_description', 10, 2);

/**
 * Changes comment form default fields.
 */
function twentynineteen_comment_form_defaults( $defaults ) {
	$comment_field = $defaults['comment_field'];

	// Adjust height of comment form.
	$defaults['comment_field'] = preg_replace( '/rows="\d+"/', 'rows="5"', $comment_field );

	return $defaults;
}
add_filter( 'comment_form_defaults', 'twentynineteen_comment_form_defaults' );


/**
 * Returns true if image filters are enabled on the theme options.
 */
function mahay_image_filters_enabled() {
	return 0 !== get_theme_mod( 'image_filter', 1 );
}

/**
 * Page title
 */
function mahay_page_title() {
	global $post;
	$tag = 'h1';
	$term = get_queried_object();
	$title =  get_the_title() ;
	$cusom_titre = get_field('cusom_titre');
	$cusom_s_titre = get_field('cusom_s_titre');
	$show_custom = get_field('afficher_un_titre_personnaliser');
	$titre = ($show_custom == true && !empty($cusom_titre)) ? strip_tags($cusom_titre) : $title;
	$s_titre = !empty($cusom_s_titre) ? $cusom_s_titre : __('<b>Mahay</b>Expédition©', 'mahay_expedition');
    echo '<'.$tag.' class="titre wow fadeInUp" data-wow-delay="600ms">'.$titre.'&nbsp;<span class="mahay-2">'.$s_titre.'</span></'.$tag.'>';
}

/**
 * Show Date Of Post
 */
function mahay_the_date(){
	echo '<b>'.get_the_date('d').'</b>&nbsp;'.ucfirst(date_i18n( 'F', strtotime(get_the_date('dd/MM/YY')) ));
}

/**
 * Function Display pagination
 */
function mahay_pagination($query="") {
    global $wp_query;
    ob_start();
    $current = max( 1, absint( get_query_var( 'paged' ) ) );
    if(is_array($query)){
        $total_post = count($query);
        $per_page = get_option('posts_per_page');
        $total = ceil( $total_post / $per_page);
    }else{
        $total = !empty($query) ? $query->max_num_pages : $wp_query->max_num_pages;            
    }

    $pagination = paginate_links( array(
        'base' => str_replace( PHP_INT_MAX, '%#%', esc_url( get_pagenum_link( PHP_INT_MAX ) ) ),
        'format' => '?paged=%#%',
        'current' => $current,
        'total' => $total,
        'type' => 'array',
        'prev_text' => '&laquo;',
        'next_text' => '&raquo;',
    ) );
    
    if ( ! empty( $pagination ) ) :
    ?>           
        <div class="pagination">
            <ul>
                <?php foreach ( $pagination as $key => $page_link ) : ?>
                    <li class="paginated_link<?php if ( strpos( $page_link, 'current' ) !== false ) { echo ' active'; } ?>"><?php echo $page_link ?></li>
                <?php endforeach ?>
            </ul>
        </div>
    <?php
    endif;
    $links = ob_get_clean();
    echo apply_filters( 'sa_bootstap_paginate_links', $links );
}

/**
 * Convert HSL to HEX colors
 */
function language_selector_flags(){
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    if(!empty($languages)){
        foreach($languages as $l){
            if(!$l['active']){
            	$img = $l['language_code'] == 'en' ? get_theme_file_uri('images/lang-en.jpg') : $l['country_flag_url'];
            	echo '<a href="'.$l['url'].'" class="lang">
            			<img src="'.$img.'" height="12" alt="'.$l['language_code'].'" width="18" />
            		</a>';
            }
        }
    }
}

/**
 * Convert HSL to HEX colors
 */
function mahay_hsl_hex( $h, $s, $l, $to_hex = true ) {

	$h /= 360;
	$s /= 100;
	$l /= 100;

	$r = $l;
	$g = $l;
	$b = $l;
	$v = ( $l <= 0.5 ) ? ( $l * ( 1.0 + $s ) ) : ( $l + $s - $l * $s );
	if ( $v > 0 ) {
		$m;
		$sv;
		$sextant;
		$fract;
		$vsf;
		$mid1;
		$mid2;

		$m       = $l + $l - $v;
		$sv      = ( $v - $m ) / $v;
		$h      *= 6.0;
		$sextant = floor( $h );
		$fract   = $h - $sextant;
		$vsf     = $v * $sv * $fract;
		$mid1    = $m + $vsf;
		$mid2    = $v - $vsf;

		switch ( $sextant ) {
			case 0:
				$r = $v;
				$g = $mid1;
				$b = $m;
				break;
			case 1:
				$r = $mid2;
				$g = $v;
				$b = $m;
				break;
			case 2:
				$r = $m;
				$g = $v;
				$b = $mid1;
				break;
			case 3:
				$r = $m;
				$g = $mid2;
				$b = $v;
				break;
			case 4:
				$r = $mid1;
				$g = $m;
				$b = $v;
				break;
			case 5:
				$r = $v;
				$g = $m;
				$b = $mid2;
				break;
		}
	}
	$r = round( $r * 255, 0 );
	$g = round( $g * 255, 0 );
	$b = round( $b * 255, 0 );

	if ( $to_hex ) {

		$r = ( $r < 15 ) ? '0' . dechex( $r ) : dechex( $r );
		$g = ( $g < 15 ) ? '0' . dechex( $g ) : dechex( $g );
		$b = ( $b < 15 ) ? '0' . dechex( $b ) : dechex( $b );

		return "#$r$g$b";

	}

	return "rgb($r, $g, $b)";
}

/**
 * Determines if post thumbnail can be displayed.
 */
function twentynineteen_can_show_post_thumbnail() {
	return apply_filters( 'twentynineteen_can_show_post_thumbnail', ! post_password_required() && ! is_attachment() && has_post_thumbnail() );
}

function get_query_partenaire(array $ids){
	$args = array(
        'post_type' => 'partenaires',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'groups',
                'field'    => 'term_id',
                'terms'    => $ids,
            ),
        ),
    );
    return new WP_Query($args);
}