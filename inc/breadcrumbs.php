<?php

/**

 * Display The Breadcrulmb

 */

function the_breadcrumb() {

    echo '<ul id="crumbs">';

    if (!is_home()) {

        echo '<li><a title="'.get_bloginfo('sitename').'" href="';

        echo get_option('home');

        echo '">';

        echo __('Accueil ','mahay_expedition');

        echo "</a></li>";

        if (is_category() || is_single() || is_archive()) {

            $post_type = get_post_type();

            if(is_tax('themes')){
                
                $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

                $name =  __('Nos circuits', 'mahay_expedition');
                 echo '<li class="active"><a href="javascript:void(0)" title="' . $term->name . '">' . $term->name . '</a></li>';

            }else if($post_type != 'post') {

                if($post_type=="actions"){
                    echo '<li class="active"><a title="nos-actions" href="'.get_permalink(3203).'"> Nos actions';

                    echo '</a></li> ';
                }
                if($post_type=="projets"){
                    echo '<li class="active"><a title="nos-projets" href="'.get_permalink(3223).'"> Nos projets à venir';

                    echo '</a></li> ';
                }

                /*global $post;

                $taxs = get_the_terms( $post->ID,  'themes' );

                $themes = $taxs[0];

                echo '<li><a href="' . get_term_link($themes->term_id, 'themes') . '" title="' . $themes->name . '">' . $themes->name . '</a></li>';*/

            }else{

                echo '<li>';

                the_category(' </li>');

            }

            if (is_single()) {

                echo "<li class='active'><a href='javascript:void(0)'>";

                the_title();

                echo '</a></li>';

            }

            if( is_archive() ){
                $post_type = get_post_type();
                if($post_type == 'partenaires'){
                    $name = __('Nos partenaires', 'mahay_expedition');
                }else{
                    $name = ucfirst($post_type);
                }
                echo "<li class='active'><a href='javascript:void(0)'>".$name."</a></li>";
            }

        } elseif (is_page()) {

            echo '<li class="active"><a title="'.get_the_title().'" href="'.get_the_permalink().'">';

            echo the_title();

            echo '</a></li>';

        }elseif (is_404()) { echo '<li class="active"><a>' . __('Erreur 404', 'mahay_expedition') . '</a></li>'; }



        elseif (is_search()) {echo '<li class="active"><a>'.__('Résultats de la recherche', 'mahay_expedition').'</a></li>';}

    }

    elseif(is_home()){

        echo '<li><a title="'.get_bloginfo('sitename').'" href="';

        echo get_option('home');

        echo '">';

        echo __('Accueil ','mahay_expedition');

        echo "</a></li>";

        echo '<li class="active">'.__('Blog Archives', 'mahay_expedition').'</li>';

    }

    elseif (is_tag()) {single_tag_title();}

    elseif (is_day()) {echo"<li>".__('Archive pour', 'mahay_expedition'); the_time('F jS, Y'); echo'</li>';}

    elseif (is_month()) {echo"<li>".__('Archive pour', 'mahay_expedition'); the_time('F, Y'); echo'</li>';}

    elseif (is_year()) {echo"<li>".__('Archive pour', 'mahay_expedition'); the_time('Y'); echo'</li>';}

    elseif (is_author()) {echo"<li>".__('Archive de l\'auteur', 'mahay_expedition'); echo'</li>';}

    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>".__('Blog Archives', 'mahay_expedition'); echo'</li>';}

    elseif (is_search()) {echo"<li>".__('Résultats de la recherche', 'mahay_expedition'); echo'</li>';}

    elseif (is_404()) { echo '<li>' . __('Erreur 404', 'mahay_expedition') . '</li>'; }

echo '</ul>';

}