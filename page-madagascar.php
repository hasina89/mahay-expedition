<?php 
	/* Template name: Page Madagascar */  
	get_header(); 
?>
<div class="wrapper">
    <div class="blcIntro">
        <?php mahay_page_title() ;
        if(have_posts()) : while(have_posts()) : the_post(); ?>
        <div class="wow fadeInUp" data-wow-delay="1200ms">
            <?php the_content() ?>
        </div>
        <?php endwhile; endif; ?>
    </div>
    <div class="slideMada">
        <?php if(have_rows('faune')) : ?>
        <div class="content-1">
            <h2 class="title wow fadeInLeft" data-wow-delay="1800ms"><?php the_field('faune_titre') ?></h2>
            <div class="slideFaune" id="slideFaune">
                <?php while(have_rows('faune')): the_row(); ?>
                <div class="item clr"> 
                    <div class="blcLeft wow fadeInLeft" data-wow-delay="1800ms">
                        <div class="img">
                            <?php 
                                $img = get_sub_field('image'); 
                                $image = !empty($img['sizes']['img_team']) ? $img['sizes']['img_team'] : get_theme_file_uri('images/img-team-2.jpg');
                            ?>
                            <img src="<?php echo $image ?>" alt="<?php bloginfo('name') ?>">
                        </div>
                    </div>
                    <div class="blcRight wow fadeInRight" data-wow-delay="1800ms">
                        <div class="text"><?php the_sub_field('description') ?></div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
        <?php endif; if(have_rows('flore')) : ?>
        <div class="content-2">
            <h2 class="title wow fadeInRight" data-wow-delay="400ms"><?php the_field('flore_titre') ?></h2>
            <div class="slideFlore" id="slideFlore">
                <?php while(have_rows('flore')): the_row(); ?>
                <div class="item clr"> 
                    <div class="blcLeft wow fadeInRight" data-wow-delay="400ms">
                        <div class="img">
                            <?php 
                                $img = get_sub_field('image'); 
                                $image = !empty($img['sizes']['img_team']) ? $img['sizes']['img_team'] : get_theme_file_uri('images/img-team-2.jpg');
                            ?>
                            <img src="<?php echo $image ?>" alt="<?php bloginfo('name') ?>">
                        </div>
                    </div>
                    <div class="blcRight wow fadeInLeft" data-wow-delay="400ms">
                        <div class="text"><?php the_sub_field('description') ?></div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <div class="blcGeologie wow fadeInUp" data-wow-delay="200ms">
        <h2 class="titre"><?php the_field('geo_titre') ?></h2>
        <div class="entry-content"><?php the_field('description') ?></div>
    </div>
    <div class="blcrepereGeo wow fadeInUp" data-wow-delay="1000ms">
        <h3 class="s-titre"><?php the_field('geo_titre_2') ?></h3>
        <div class="entry-content"><?php the_field('contenu') ?></div>
    </div>
    <div class="blcGalerie wow fadeIn" data-wow-delay="1000ms">
        <?php get_template_part('template-parts/content/galerie') ?>
    </div>
</div>
<?php get_footer(); ?>