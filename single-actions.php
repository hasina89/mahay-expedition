<?php 
	get_header(); 
?>
<div class="wrapper">
    <div class="blcIntro">
        <?php mahay_page_title() ;
        if(have_posts()) : while(have_posts()) : the_post(); ?>
        <div class="wow fadeInUp" data-wow-delay="1200ms">
            <?php the_content() ?>
        </div>
        <?php endwhile; endif; ?>
    </div>
</div>
<?php get_footer(); ?>