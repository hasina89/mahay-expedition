<?php get_header(); ?>

	<section id="primary" class="content-area">
		<div class="contentActu">
			<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content/single', 'post' );

				endwhile; // End of the loop.

				get_sidebar();
			?>
		</div><!-- #wrapper -->
	</section><!-- #primary -->

<?php get_footer(); ?>