<?php if( is_front_page() ) : if(have_rows('diaporama')) : ?>
    <div class="blocSlider"> 
        <div id="slider">
            <?php 
                while(have_rows('diaporama')) : 
                    the_row();
                    $image = get_sub_field('image');
                    $img = isset($image['sizes']['banner']) ? $image['sizes']['banner'] : $image['url'];
            ?>
            <div class="banner banner1" style="background-image:url('<?php echo esc_url($img ) ?>')">
                <div class="textBanner">
                    <div class="content">
                        <div>
                            <span class="title"><?php the_sub_field('titre') ?></span>
                            <div class="text">
                                <?php the_sub_field('description') ?>
                            </div>
                            <div class="blcLink">
                                <a href="blcService" rel="nofollow" class="link scroll" title="<?php _e('Découvrez nos circuits', 'mahay_expedition') ?>"><?php _e('Découvrez nos circuits', 'mahay_expedition') ?></a>
                                <a href="<?php echo get_the_permalink(16) ?>" class="link contact" title="<?php _e('Contactez-nous', 'mahay_expedition') ?>"><?php _e('contactez-nous', 'mahay_expedition') ?></a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
        <a href="<?php echo add_query_arg( array('objet' => 'depart' ), home_url('/contact') ); ?>" class="link linkDepart" title="<?php _e('Profitez de nos départs garantis', 'mahay_expedition') ?>"><div><?php _e('Profitez de nos départs garantis', 'mahay_expedition')?></div></a>
    </div>
<?php 
    endif; 
    else : 
        $banner = get_field('banner');
?>
    <div id="wrapBanner">
        <?php if(!empty($banner)) : $bg = !empty($banner['sizes']['banner_page']) ? $banner['sizes']['banner_page'] : $banner['url']; ?>
            <div class="banner banner-page" style="background-image: url('<?php echo esc_url($bg) ?>')"></div>
        <?php else:  ?>
            <div class="banner <?php banner_classe() ?>"></div>
        <?php endif; ?>
        <div class="bredcrumb">
            <div class="wrapper">
                <span><?php _e('Vous êtes ici :', 'mahay_expedition') ?></span>
                <?php the_breadcrumb() ?>
            </div>
        </div>
    </div>
<?php endif; ?>