<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

	get_header();
	if ( have_posts() ) : 
?>
		<section id="primary" class="content-area">
			<div class="blcPartenaire archive-partenaire">
				<div class="wrapper" style="padding-bottom: 0; padding-top: 0;">
					<div class="container">
						<div class="slideActu clr">
							<?php 
								while ( have_posts() ) {
									the_post();
									get_template_part( 'template-parts/content/item', 'actus');
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</section>	
	<?php else: ?>
		<section id="primary" class="content-area">
			<main id="main" class="pageMada">			
				<div class="wrapper">
					<div class="blcIntro">
					<header class="page-header">
						<h1 class="titre"><?php _e( 'Aucun résultat', 'mahay_expedition' ); ?></h1>
					</header><!-- .page-header -->
					<div class="page-content">
						<p><?php _e( 'Désolé, mais rien ne correspond à votre recherche. Veuillez réessayer avec des mots différents.', 'mahay_expedition' ); ?></p>
						<div class="circuit"><div class="blcFiltre" style="padding-top: 1em"><?php get_search_form(); ?> </div></div>
					</div><!-- .page-content -->
				</div>
				</div>
			</main><!-- #main -->
		</section><!-- #primary -->
	<?php endif; ?>
<?php
get_footer();