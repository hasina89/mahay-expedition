var $ = jQuery.noConflict();

$(document).ready(function() {	
	// MENU MOBILE //
	$(".wrapMenuMobile").click(function() {
		$(this).toggleClass('active');
		$(".menuMobile").toggleClass('active');
		$(".menu ul").fadeToggle();
		$(".sub").css('display','none');
		$(".menu li i").removeClass('active');
		$('.listService').toggleClass('z-index')
	});	
	$(".menu li i").click(function() {
		$(this).toggleClass('active');
		$(".menu").find('.sub' ).slideUp();
		if($(this).hasClass("active")){
			$(".menu li i").removeClass('active')
			$(this).next().slideToggle();
			$(this).toggleClass('active');
		}
	});
	


	$('.menu-item-has-children > a').click(function(){
	    if(window.matchMedia("(min-width:1201px)").matches) {
	        return false;
	    }else{
	        $(this).toggleClass('dropdown').next().slideToggle();
	        return false;
	    }
	})

	$(".language").click(function() {
		$(".language li").next().slideToggle('active');
		
	});	


	// SLIDER HOME //
	$('#slider ').slick({
			dots:false,
			infinite:true,
			autoplaySpeed:4000,
			speed:1000,
			fade:true,
			arrows:false,
			autoplay:true,
			pauseOnHover:false,
			cssEase:'linear',
	});

	var sldService1 = $('#listService').slick({
			dots:false,
			infinite:true,
			autoplaySpeed:4000,
			speed:1200,
			arrows:false,
			autoplay:true,
			pauseOnHover:false,
			slidesToShow: 1,
	        slidesToScroll: 1,
	        pauseOnHover:true,
			cssEase:'linear',
			asNavFor: '#listService-2 ,#listService-3',
			responsive: [
				{ 
				  breakpoint: 769,
				   settings: {
					slidesToShow: 1,
					adaptiveHeight:true,
					autoplay:false,
					fade:true,
					arrows:true,
				  }
				},
			]
			
	});
	var sldService2 =$('#listService-2').slick({
			dots:false,
			infinite:true,
			autoplaySpeed:4200,
			speed:1000,
			arrows:true,
			pauseOnHover:false,
			slidesToShow: 1,
	        slidesToScroll: 1,
	        pauseOnHover:true,
	        // initialSlide:1,
			cssEase:'linear',
			prevArrow: '#prevSlide',
			nextArrow: '#nextSlide',
			asNavFor: '#listService ,#listService-3',
			responsive: [
				{ 
				  breakpoint: 769,
				   settings: {
					arrows:false,
				  }
				},
			]
			
	});
	var sldService3 = $('#listService-3').slick({
			dots:false,
			infinite:true,
			autoplaySpeed:4400,
			speed:800,
			arrows:false,
			pauseOnHover:false,
			slidesToShow: 1,
	        slidesToScroll: 1,
	        pauseOnHover:true,
	        // initialSlide: 2,
			cssEase:'linear',
			asNavFor: '#listService-1 ,#listService-2',
	});



	$('#blcService .item').hover(
		function(){
			sldService1.slick('slickPause');
			sldService2.slick('slickPause');
			sldService3.slick('slickPause');
		},
		function(){
			sldService1.slick('slickPlay');
			sldService2.slick('slickPlay');
			sldService3.slick('slickPlay');			
		}
	);

	$('#listGalerie').slick({
		dots:false,
		infinite:true,
		autoplaySpeed:4000,
		speed:500,
		arrows:true,
		autoplay:true,
		pauseOnHover:false,
		slidesToShow: 3,
        slidesToScroll: 1,
		cssEase:'linear',
		responsive: [
			{ 
			  breakpoint: 768,
			   settings: {
				slidesToShow: 2,
			  }
			}, 
			{ 
			  breakpoint: 601,
			   settings: {
				slidesToShow: 1,
			  }
			},   
		 ]
	});
	$('#listActu').slick({
		dots:false,
		infinite:true,
		autoplaySpeed:4000,
		speed:500,
		arrows:false,
		autoplay:true,
		pauseOnHover:false,
		slidesToShow: 2,
        slidesToScroll: 1,
		cssEase:'linear',
		responsive: [
			{ 
			  breakpoint: 768,
			   settings: {
				slidesToShow: 1,
				fade:true,
			  }
			},  
		 ]
	});
	$('#slideTestimonial').slick({
		dots:false,
		infinite:true,
		autoplaySpeed:4000,
		speed:500,
		arrows:true,
		autoplay:true,
		pauseOnHover:false,
		slidesToShow: 1,
        slidesToScroll: 1,
        fade:true,
		cssEase:'linear',
		appendArrows: $('.arrowTestimonial'),
	});

	// QUI SOMMES NOUS //

	$('#slideTeam').slick({
		dots:false,
		infinite:true,
		autoplaySpeed:4000,
		speed:500,
		arrows:true,
		adaptiveHeight : false,
		autoplay:true,
		pauseOnHover:false,
		slidesToShow: 1,
	    slidesToScroll: 1,
		cssEase:'linear',
		fade:true,
		responsive: [
			{ 
			  breakpoint: 768,
			   settings: {
				slidesToShow: 1,
				fade:true,
			  }
			},  
		 ]
	});
	$('#listSignification').slick({
		dots:false,
		infinite:true,
		autoplaySpeed:4000,
		speed:500,
		arrows:false,
		autoplay:false,
		pauseOnHover:false,
		slidesToShow: 5,
	    slidesToScroll: 1,
	    focusOnSelect: true,
	    vertical: true,
	    asNavFor: '#signification' ,
		cssEase:'linear',

	});
	$('#signification').slick({
		dots:false,
		infinite:true,
		autoplaySpeed:4000,
		speed:500,
		arrows:false,
		autoplay:true,
		pauseOnHover:false,
		slidesToShow: 1,
	    slidesToScroll: 1,
		cssEase:'linear',
		asNavFor: '#listSignification' ,
		fade:true,
		responsive: [
			{ 
			  breakpoint: 768,
			   settings: {
				slidesToShow: 1,
				fade:true,
				adaptiveHeight: true,
			  }
			},  
		 ]
	});

	// ACTUALITES LISTE //
	$('#slideActu').slick({
		dots:false,
		infinite:true,
		autoplaySpeed:4000,
		speed:500,
		arrows:true,
		autoplay:true,
		pauseOnHover:false,
		slidesToShow: 3,
	    slidesToScroll: 1,
		cssEase:'linear',
		responsive: [
			{ 
			  breakpoint: 1024,
			   settings: {
				slidesToShow: 2,
				adaptiveHeight: true,
			  }
			}, 
			{ 
			  breakpoint: 768,
			   settings: {
				slidesToShow: 1,
				adaptiveHeight: true,
			  }
			},  
		 ]
	});
	// CIRCUIT DETAIL //
	$('#slideParcours').slick({
		dots:false,
		infinite:true,
		autoplaySpeed:4000,
		speed:500,
		arrows:true,
		autoplay:true,
		pauseOnHover:false,
		slidesToShow: 1,
	    slidesToScroll: 1,
		cssEase:'linear',
		fade:true,
		responsive: [
			{ 
			  breakpoint: 768,
			   settings: {
				slidesToShow: 1,
				fade:true,
			  }
			},  
		 ]
	});
	$('#slideCircuit').slick({
		dots:false,
		infinite:false,
		autoplaySpeed:4000,
		speed:500,
		arrows:true,
		autoplay:false,
		pauseOnHover:false,
		slidesToShow: 3,
	    slidesToScroll: 1,
	    vertical: false,
		cssEase:'linear',
		responsive: [
			{ 
			  breakpoint: 1200,
			   settings: {
				slidesToShow: 2,
			  }
			},
			{ 
			  breakpoint: 900,
			   settings: {
				slidesToShow: 1,
				adaptiveHeight:false,
			  }
			},   
		 ]
	});
	$('.listeCircuit').slick({
		dots:false,
		infinite:false,
		autoplaySpeed:4000,
		speed:500,
		arrows:true,
		autoplay:true,
		pauseOnHover:false,
		slidesToShow: 3,
	    slidesToScroll: 1,
	    vertical: false,
		cssEase:'linear',
		responsive: [
			{ 
			  breakpoint: 1200,
			   settings: {
				slidesToShow: 2,
			  }
			},
			{ 
			  breakpoint: 900,
			   settings: {
				slidesToShow: 1,
				adaptiveHeight:false,
			  }
			},   
		 ]
	});

	// MADAGASCAR //
	$('#slideFaune').slick({
		dots:false,
		infinite:true,
		autoplaySpeed:4000,
		speed:500,
		arrows:true,
		autoplay:true,
		pauseOnHover:false,
		slidesToShow: 1,
	    slidesToScroll: 1,
		cssEase:'linear',
		fade:true,
		responsive: [
			{ 
			  breakpoint: 768,
			   settings: {
				slidesToShow: 1,
				fade:true,
			  }
			},  
		 ]
	});
	$('#slideFlore').slick({
		dots:false,
		infinite:true,
		autoplaySpeed:4000,
		speed:500,
		arrows:true,
		autoplay:true,
		pauseOnHover:false,
		slidesToShow: 1,
	    slidesToScroll: 1,
		cssEase:'linear',
		fade:true,
		responsive: [
			{ 
			  breakpoint: 768,
			   settings: {
				slidesToShow: 1,
				fade:true,
			  }
			},  
		 ]
	});

	// PARTENAIRES//
	$('#slidePartenaires').slick({
		dots:false,
		infinite:true,
		autoplaySpeed:4000,
		speed:500,
		arrows:true,
		autoplay:true,
		pauseOnHover:false,
		slidesToShow: 1,
	    slidesToScroll: 1,
		cssEase:'linear',
		fade:true,
		adaptiveHeight:true,
		responsive: [
			{ 
			  breakpoint: 768,
			   settings: {
				slidesToShow: 1,
				fade:true,
			  }
			},  
		 ]
	});





	
	// SCROLL //
	$(".scroll").click(function() {
		var c = $(this).attr("href");
		$('html, body').animate({ scrollTop: $("#" + c).offset().top-110 }, 1000, "linear");
		return false;
	});

	$(".ethique").click(function(e) {
		e.preventDefault();
		var c = $(this).attr("href");
		if($(c).length){
			$('html, body').animate({ scrollTop: $(c).offset().top }, 1000, "linear");
		}
		return false;
	});

	  /* wow animation */
    new WOW({
        mobile:false, 
    }).init() 

    // FORM
    $('.placeholder').click(function() {
	  $(this).siblings('input').focus();
	});
	$('.form-control').focus(function() {
		if(!$(this).hasClass('form-select'))
	  		$(this).siblings('.placeholder').hide();
	});
	$('.form-control').blur(function() {
	  	var $this = $(this);
	  	if ($this.val() == 0)
	    	$(this).siblings('.placeholder').show();
	});

	$('.form-select').change(function() {
	  	var $this = $(this);
	  	$this.siblings('.placeholder').hide();
	  	if ($this.val().length == 0)
	    	$(this).siblings('.placeholder').show();
	});
	$('.form-control').blur();


});

$(window).load(function(){
	$('.form-control').each(function() {
		var $this = $(this);
		if ($this.val()){
	  		$(this).siblings('.placeholder').hide();
		}
	});
});

/*(function($){
	var $widget = $('.SW_SimpleMakeAppointment');
	$('.form-control', $widget).blur(function(){
		if($(this).val() || $(this).text()){
			$(this).parent('.form-group').find('label:not(.error)').addClass('fixe')
		}else{
			$(this).parent('.form-group').find('label:not(.error)').removeClass('fixe')				
		}
	});

	$(document).on('keypress', '.error .form-control', function(e){
		var $widget = $(this).parents('.SW_SimpleMakeAppointment');
		$('.response-output', $widget).remove();
		$('.btn-submit', $widget).show();
	});

	// validation	
	if($widget.length){
		var $v1 = $('.SW_SimpleMakeAppointment.style1');
		var $v2 = $('.SW_SimpleMakeAppointment.style2');

		// V1
		var $form_v1 = $('form', $v1);
		if($form_v1.length){
			var validation = $form_v1.validate({
				onfocusout: false,
				rules:{
					s1_tel :{ number : true}
				},
				highlight: function(element, errorClass, validClass) {
		          var $el = elem = $(element);
		          $el.parents('.form-group').addClass('error');
		        },
		        unhighlight: function(element, errorClass, validClass) {
		          var $el = elem = $(element);
		          $el.parents('.form-group').removeClass('error');
		        },
		        invalidHandler: function(form, validator){
		        	$('.response-output', $form_v1).remove();
		        	var $btn = $('.btn-submit', $form_v1);
		        	var $output = $('<div class="response-output"></div>');
		        	$btn.hide();
		        	$output.text($btn.attr('data-error')).addClass('o-error').insertAfter($btn).fadeIn();
		        },
		        submitHandler: function(form){
		        	$('.response-output', $form_v1).remove();
		        	var $btn = $('.btn-submit', $form_v1);
		        	var $output = $('<div class="response-output"></div>');
		        	$btn.hide();
		        	$output.text($btn.attr('data-success')).addClass('o-success').insertAfter($btn).fadeIn();
		        	return false;
		        }
			});
		}

		
	}

	$('.numericCh').on('keypress', function(e){ 
	  return e.metaKey || 
		e.which <= 0 || 
		e.which == 8 || 
		/[0-9]/.test(String.fromCharCode(e.which));  
		
	});

	$('.datepicker').datetimepicker({
		format: 'DD/MM/YYYY',
	});

	$('.timepicker').datetimepicker({
		format: 'H:mm',
	});

})(jQuery);*/

// TOURISME //
	$('.sliderPartenaire').slick({
		dots:false,
		infinite:false,
		autoplaySpeed:4000,
		speed:500,
		arrows:true,
		autoplay:true,
		pauseOnHover:false,
		slidesToShow: 3,
	    slidesToScroll: 3,
		cssEase:'linear',
		adaptiveHeight:true,
		responsive: [
			{ 
			  breakpoint: 1200,
			   settings: {
				slidesToShow: 2,
				 slidesToScroll: 1,
				 autoplay:true,
			  }
			},
			{ 
			  breakpoint: 900,
			   settings: {
				slidesToShow: 1,
				 slidesToScroll: 1,
				adaptiveHeight:false,
				autoplay:true,
			  }
			},   
		 ]
	});


	// scroll logo
	$(window).scroll(function() {
		var scroll = $(window).scrollTop();		
	    if (scroll > 110) { 
	    	$('.linkDepart').addClass('fixed');
	    } 		
	    else {
	    	$('.linkDepart').removeClass('fixed');
	    }
	})

    jQuery(document).ready(function($){
    	if($('.bredcrumb').length){    		
	        setTimeout(function(){
	            $('html, body').animate({ scrollTop: $('.bredcrumb').offset().top }, 1000, "linear");
	        }, 2000);
    	}

    });
