<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="pageMada">			
			<div class="wrapper">
				<div class="blcIntro">
				<header class="page-header">
					<h1 class="titre"><?php _e( 'Aïe ! Cette page est introuvable.', 'mahay_expedition' ); ?></h1>
				</header><!-- .page-header -->
				<div class="page-content">
					<p><?php _e( 'Apparemment, rien n’a été trouvé à cette adresse. Essayez avec une recherche ??', 'mahay_expedition' ); ?></p>
				</div><!-- .page-content -->
			</div>
			</div>
		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();